<?php
Route::get('/clear-cache', function() {
	Artisan::call('cache:clear');
	Artisan::call('config:clear');
	Artisan::call('view:clear');
	Artisan::call('route:clear');

	return "done";
});


/* DASHBOARD */
Route::group(['prefix'=>'/dashboard/'], function() {
	$dashboard = 'DashController@';

	//Pages
	Route::group(['prefix'=>'/pages/'], function() {
		$controller = 'PagesController@';

		Route::get('fetch/{url}', $controller.'fetch');
		Route::post('store/{url}', $controller.'store');


		//PÁGINAS ( BANNERS E ETC )
		Route::group(['prefix'=>'homepage/'], function() {
			//GET ALL DATA
			Route::get('get', 'Pages_HomeController@get');

			//SLIDER ONE
			Route::post('hero/store', 'Pages_HomeController@storeSlider');
			Route::post('hero/update/{id}', 'Pages_HomeController@updateSlider');
			Route::delete('removeSlider/{id}', 'Pages_HomeController@removeSlider');

			Route::post('uploadFileSlider', 'Pages_HomeController@uploadFileSlider');
			Route::post('removeFileSlider', 'Pages_HomeController@removeFileSlider');

			//TEXTOS
			Route::put('text/update', 'Pages_HomeController@updateText');

			//SLIDER TWO
			Route::post('slider_two/store', 'Pages_HomeController@sliderTwo');
			Route::post('slider_two/update/{id}', 'Pages_HomeController@updateSliderTwo');
			Route::delete('removeSliderTwo/{id}', 'Pages_HomeController@removeSliderTwo');

			//SLIDER VIDEO
			Route::post('sliderVideo/store', 'Pages_HomeController@sliderVideo');
			Route::post('sliderVideo/update/{id}', 'Pages_HomeController@updateSliderVideo');
			Route::delete('removeSliderVideo/{id}', 'Pages_HomeController@removeSliderVideo');

			//SLIDER DESIGNERS
			Route::put('slider/desginers', 'Pages_HomeController@designers');
			Route::delete('slider/desginers/{ordem}', 'Pages_HomeController@removeSliderDesigners');
		});


		//CATEGORIAS
		Route::group(['prefix'=>'categories/'], function() {
			$controller = 'CategoriesController@';

			//GET ALL DATA
			Route::get('get', $controller.'get');
		});
	});


	//MANUFACTURERS ( INFO's e Imagens )
	Route::group(['prefix'=>'/manufacturers/'], function() {
		Route::get('get', 'ManufacturersController@get');
		Route::get('fetch/{manufacturer}', 'ManufacturersController@fetch');

		Route::post('update/{manufacturer}', 'ManufacturersController@update');
		Route::put('removeImage/{value}/{manufacturer}', 'ManufacturersController@removeImage');

		Route::post('upload/image/{id}', 'ManufacturersController@upload');
	});


	//TRANSLATE
	Route::group(['prefix'=>'/translate/'], function() {
		$controller = 'TranslateController@';

		Route::get('get', $controller.'get');
		Route::get('fetch/{translate}', $controller.'fetch');

		Route::post('store/', $controller.'store');
		Route::post('update/{translate}', $controller.'update');
		Route::delete('delete/{translate}', $controller.'delete');
	});


	//NEWSLETTERS
	Route::group(['prefix'=>'/newsletters/'], function() {
		$controller = 'NewslettersController@';

		Route::get('get', $controller.'get');
		Route::get('fetch/{newsletter}', $controller.'fetch');

		Route::post('store/', $controller.'store');
		Route::post('update/{newsletter}', $controller.'update');
		Route::delete('delete/{newsletter}', $controller.'delete');
	});

	//CATALOGS
	Route::group(['prefix'=>'/catalogs/'], function() {
		$controller = 'CatalogsController@';

		Route::get('get', $controller.'get');
		Route::get('fetch/{catalog}', $controller.'fetch');

		Route::post('store/', $controller.'store');
		Route::post('update/{catalog}', $controller.'update');
		Route::delete('delete/{catalog}', $controller.'delete');
	});


	//AMBIENTES
	Route::group(['prefix'=>'/ambientes/'], function() {
		$controller = 'AmbientesController@';

		Route::get('get', $controller.'get');
		Route::get('fetch/{id}', $controller.'fetch');
		Route::post('update/{id}', $controller.'update');
		/*

		Route::post('store/', $controller.'store');
		Route::post('update/{catalog}', $controller.'update');
		Route::delete('delete/{catalog}', $controller.'delete');*/
	});


});



Route::get('{any}', function () { 
	return view('index'); 
})->where('any','.*');