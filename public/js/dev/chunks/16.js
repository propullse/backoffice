(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[16],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/translate/form.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/translate/form.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['id'],
  components: {},
  data: function data() {
    return {
      url: window.baseUrl,
      loader: true,
      tab: 1,
      action: null,
      route: null,
      rules: {
        required: [function (value) {
          return !!value || "Campo obrigatório.";
        }]
      },
      form: {
        value: {}
      },
      errors: []
    };
  },
  mounted: function mounted() {
    if (this.id) {
      this.action = 'update';
      this.route = "/dashboard/translate/update/" + this.id;
      this.get();
    } else {
      this.message = 'Adicionar';
      this.action = 'store';
      this.route = "store";
      this.loader = false;
    }
  },
  methods: {
    get: function get() {
      var _this = this;

      axios.get('/dashboard/translate/fetch/' + this.id).then(function (response) {
        _this.form = response.data;
        console.log(_this.form);
        _this.message = 'Alterar "' + _this.form.value['pt'] + '"';
        _this.loader = false;
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    save: function save() {
      var _this2 = this;

      this.$refs.form.validate();
      axios.post(this.route, this.form).then(function (response) {
        _this2.loader = false;

        _this2.$refs.flash.handler(true, 'success', 'Guardado com sucesso!');

        setTimeout(function () {
          return window.location.href = "/dashboard/translate";
        }, 1500);
      })["catch"](function (error) {
        if (error.response.status == 422) {
          _this2.$refs.flash.handler(true, 'error', 'Campos em falta!');

          _this2.errors = [];
          Object.keys(error.response.data.errors).map(function (key) {
            _this2.errors.push(key);
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/translate/form.vue?vue&type=template&id=43f6cbbf&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/translate/form.vue?vue&type=template&id=43f6cbbf& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.loader == false
    ? _c(
        "div",
        [
          _c("Flash", { ref: "flash" }),
          _vm._v(" "),
          _c("app-card", { staticClass: "col-sm-12" }, [
            _c("h3", [_vm._v(_vm._s(_vm.message))])
          ]),
          _vm._v(" "),
          _c(
            "v-form",
            { ref: "form" },
            [
              _c(
                "app-card",
                { staticClass: "col-sm-12" },
                [
                  _c("v-text-field", {
                    attrs: { label: "Nome" },
                    model: {
                      value: _vm.form.id,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "id", $$v)
                      },
                      expression: "form.id"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                { staticClass: "col-sm-12" },
                _vm._l(this.$store.getters.languages, function(item) {
                  return _c("v-text-field", {
                    key: "item" + item.locale,
                    staticClass: "pb-3",
                    attrs: { label: item.name },
                    model: {
                      value: _vm.form.value[item.locale],
                      callback: function($$v) {
                        _vm.$set(_vm.form.value, item.locale, $$v)
                      },
                      expression: "form.value[item.locale]"
                    }
                  })
                }),
                1
              ),
              _vm._v(" "),
              _c("app-card", { staticClass: "col-sm-12" }, [
                _c(
                  "div",
                  { staticClass: "col-sm-12 pl-0 ml-0 pt-3" },
                  [
                    _c(
                      "v-btn",
                      {
                        attrs: { color: "success" },
                        on: {
                          click: function($event) {
                            $event.stopPropagation()
                            return _vm.save()
                          }
                        }
                      },
                      [_vm._v("Guardar")]
                    )
                  ],
                  1
                )
              ])
            ],
            1
          )
        ],
        1
      )
    : _c("div", [_c("Progress")], 1)
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/dashboard/translate/form.vue":
/*!*********************************************************!*\
  !*** ./resources/js/views/dashboard/translate/form.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _form_vue_vue_type_template_id_43f6cbbf___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./form.vue?vue&type=template&id=43f6cbbf& */ "./resources/js/views/dashboard/translate/form.vue?vue&type=template&id=43f6cbbf&");
/* harmony import */ var _form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/translate/form.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _form_vue_vue_type_template_id_43f6cbbf___WEBPACK_IMPORTED_MODULE_0__["render"],
  _form_vue_vue_type_template_id_43f6cbbf___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/translate/form.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/translate/form.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/views/dashboard/translate/form.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./form.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/translate/form.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/translate/form.vue?vue&type=template&id=43f6cbbf&":
/*!****************************************************************************************!*\
  !*** ./resources/js/views/dashboard/translate/form.vue?vue&type=template&id=43f6cbbf& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_43f6cbbf___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./form.vue?vue&type=template&id=43f6cbbf& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/translate/form.vue?vue&type=template&id=43f6cbbf&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_43f6cbbf___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_43f6cbbf___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);