(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[17],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/translate/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/translate/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: [],
  components: {},
  data: function data() {
    return {
      url: window.baseUrl,
      loader: true,
      data: {},
      headers: [{
        text: "Nome",
        sortable: false
      }, {
        text: "Valor",
        sortable: false
      }, {
        text: "Opções",
        sortable: false
      }]
    };
  },
  mounted: function mounted() {
    this.get();
  },
  methods: {
    get: function get() {
      var _this = this;

      axios.get('/dashboard/translate/get').then(function (response) {
        _this.data = response.data;
        _this.loader = false;
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    confirmDelete: function confirmDelete(value) {
      this.$refs["delete"].open = true;
      this.toDelete = value;
    },
    deleteFunction: function deleteFunction() {
      var _this2 = this;

      axios["delete"]("/dashboard/translate/delete/" + this.toDelete).then(function (response) {
        _this2.$refs["delete"].open = false;

        _this2.$refs.flash.handler(true, 'success', 'Eliminado com sucesso!');

        _this2.get();
      })["catch"](function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/translate/index.vue?vue&type=template&id=bcfa3f32&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/translate/index.vue?vue&type=template&id=bcfa3f32& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.loader == false
    ? _c(
        "div",
        [
          _c("Flash", { ref: "flash" }),
          _vm._v(" "),
          _c("Delete", {
            ref: "delete",
            on: { onConfirm: _vm.deleteFunction }
          }),
          _vm._v(" "),
          _c(
            "app-card",
            { staticClass: "col-sm-12" },
            [
              _c(
                "div",
                { staticClass: "row pb-6" },
                [
                  _c(
                    "v-btn",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "primary", to: "translate/form" }
                    },
                    [_vm._v("Adicionar")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("v-data-table", {
                attrs: { headers: _vm.headers, items: _vm.data },
                scopedSlots: _vm._u(
                  [
                    {
                      key: "item",
                      fn: function(ref) {
                        var item = ref.item
                        return [
                          _c("tr", [
                            _c("td", [
                              _vm._v(
                                "\n            [ " +
                                  _vm._s(item.id) +
                                  " ]\n          "
                              )
                            ]),
                            _vm._v(" "),
                            _c("td", [
                              _vm._v(
                                "\n            " +
                                  _vm._s(
                                    item.value["pt"].substring(0, 88) + "..."
                                  ) +
                                  "\n          "
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "td",
                              [
                                _c(
                                  "router-link",
                                  {
                                    attrs: { to: "translate/form/" + item.id }
                                  },
                                  [
                                    _c(
                                      "button",
                                      {
                                        staticClass:
                                          "mx-0 v-btn v-btn--flat v-btn--icon v-btn--round theme--light v-size--default",
                                        attrs: { type: "button" }
                                      },
                                      [
                                        _c(
                                          "span",
                                          { staticClass: "v-btn__content" },
                                          [
                                            _c(
                                              "i",
                                              {
                                                staticClass:
                                                  "v-icon notranslate primary--text material-icons theme--light",
                                                attrs: { "aria-hidden": "true" }
                                              },
                                              [_vm._v("edit")]
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "mx-0 v-btn v-btn--flat v-btn--icon v-btn--round theme--light v-size--default",
                                    attrs: { type: "button" },
                                    on: {
                                      click: function($event) {
                                        return _vm.confirmDelete(item.id)
                                      }
                                    }
                                  },
                                  [
                                    _c(
                                      "span",
                                      { staticClass: "v-btn__content" },
                                      [
                                        _c(
                                          "i",
                                          {
                                            staticClass:
                                              "v-icon notranslate error--text material-icons theme--light",
                                            attrs: { "aria-hidden": "true" }
                                          },
                                          [_vm._v("close")]
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ])
                        ]
                      }
                    }
                  ],
                  null,
                  false,
                  1823082194
                )
              })
            ],
            1
          )
        ],
        1
      )
    : _c("div", [_c("Progress")], 1)
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/dashboard/translate/index.vue":
/*!**********************************************************!*\
  !*** ./resources/js/views/dashboard/translate/index.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_bcfa3f32___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=bcfa3f32& */ "./resources/js/views/dashboard/translate/index.vue?vue&type=template&id=bcfa3f32&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/translate/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_bcfa3f32___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_bcfa3f32___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/translate/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/translate/index.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/dashboard/translate/index.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/translate/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/translate/index.vue?vue&type=template&id=bcfa3f32&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/views/dashboard/translate/index.vue?vue&type=template&id=bcfa3f32& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_bcfa3f32___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=bcfa3f32& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/translate/index.vue?vue&type=template&id=bcfa3f32&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_bcfa3f32___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_bcfa3f32___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);