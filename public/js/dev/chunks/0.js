(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['data', 'manufacturers'],
  components: {},
  data: function data() {
    return {
      modal: false,
      headers: [{
        text: "Designer"
      }, {
        text: "Ordem"
      }, {
        text: "Opções"
      }],
      items: [],
      selectedToDelete: null,
      form: {},
      formValid: false,
      rules: {
        required: [function (value) {
          return !!value || "Campo obrigatório.";
        }]
      },
      errors: []
    };
  },
  mounted: function mounted() {
    this.get();
  },
  methods: {
    get: function get() {
      var _this = this;

      this.items = [];

      if (this.data.slider_three) {
        Object.keys(this.data).map(function (key) {
          _this.items.push(JSON.parse(_this.data[key]));
        });
      }
    },
    save: function save() {
      var _this2 = this;

      this.$refs.myForm.validate();
      axios.put("/dashboard/pages/homepage/slider/desginers", this.form).then(function (response) {
        _this2.$emit('updated');

        _this2.close(false);

        _this2.$refs.flash.handler(true, 'success', 'Inserido com sucesso!');
      })["catch"](function (error) {
        if (error.response.status == 422) {
          _this2.$refs.flash.handler(true, 'error', 'Campos em falta!');

          _this2.errors = [];
          Object.keys(error.response.data.errors).map(function (key) {
            _this2.errors.push(key);
          });
        }
      });
    },
    close: function close() {
      this.modal = false;
      this.form = {};
      this.$emit('updated');
    },
    confirmDelete: function confirmDelete(value) {
      this.$refs["delete"].open = true;
      this.selectedToDelete = value;
    },
    deleteFunction: function deleteFunction() {
      var _this3 = this;

      axios["delete"]("/dashboard/pages/homepage/slider/desginers/" + this.selectedToDelete).then(function (response) {
        _this3.$refs["delete"].open = false;

        _this3.$refs.flash.handler(true, 'success', 'Eliminado com sucesso!');

        _this3.$emit('updated');
      })["catch"](function (error) {
        console.log(error);
      });
    }
  },
  watch: {
    data: {
      handler: function handler() {
        this.get();
      },
      deep: true
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue2-dropzone */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['data'],
  components: {
    Dropzone: vue2_dropzone__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  data: function data() {
    var $vm = this;
    return {
      url: window.baseUrl,
      action: null,
      formValid: false,
      rules: {
        required: [function (value) {
          return !!value || "Campo obrigatório.";
        }]
      },
      positions: [{
        value: 'left',
        text: 'Esquerda'
      }, {
        value: 'center',
        text: 'Centro'
      }, {
        value: 'right',
        text: 'Direita'
      }],
      modal: false,
      removeServer: true,
      dropzone: {
        url: "/dashboard/pages/homepage/uploadFileSlider",
        dictDefaultMessage: "Arraste ou selecione os ficheiros.",
        maxFilesize: 10,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: ".jpg,.jpeg,.png",
        headers: {
          "X-CSRF-TOKEN": document.head.querySelector("[name=csrf-token]").content
        },
        //Quando faz upload com sucesso
        success: function success(file, response) {
          $vm.form.file = response;
          file.upload.name = response;
        },
        //Quando se clica em 'Remover'
        removedfile: function removedfile(file) {
          if ($vm.removeServer != false) {
            $vm.removeFile(file.upload.name, 'homepage');
          }

          file.previewElement.remove();
        },
        //Quando excede o nº ficheiros
        maxfilesexceeded: function maxfilesexceeded(file) {
          file.previewElement.remove();
        },
        //Quando falha o tipo de ficheiro .PDF || ou Erro
        error: function error(file) {
          if (file.status == "error") {
            file.previewElement.remove();
          }
        }
      },
      headers: [{
        text: "Imagem"
      }, {
        text: "Ordem"
      }, {
        text: "Opções"
      }],
      items: [],
      form: {
        file: null,
        ordem: null,
        url: {}
      },
      errors: [],
      selectedToDelete: null
    };
  },
  mounted: function mounted() {
    this.items = this.data ? this.data : [];
  },
  methods: {
    openForm: function openForm(action, item, id) {
      this.action = action;
      this.modal = true;
      this.form = item ? item : {
        file: null,
        ordem: null,
        url: {}
      };
    },
    removeFile: function removeFile(file, disk) {
      var _this = this;

      axios.post("/dashboard/pages/homepage/removeFileSlider", {
        file: file,
        disk: disk
      }).then(function (response) {
        _this.$refs.flash.handler(true, 'success', 'Eliminado!');
      })["catch"](function (error) {
        console.log(error);
      });
    },
    save: function save() {
      var _this2 = this;

      this.$refs.myForm.validate();
      var url = this.action == 'store' ? this.action : this.action + '/' + this.form.id;
      axios.post('/dashboard/pages/homepage/hero/' + url, this.form).then(function (response) {
        _this2.close(false);

        _this2.$refs.flash.handler(true, 'success', 'Inserido com sucesso!');
      })["catch"](function (error) {
        if (error.response.status == 422) {
          _this2.$refs.flash.handler(true, 'error', 'Campos em falta!');

          _this2.errors = [];
          Object.keys(error.response.data.errors).map(function (key) {
            _this2.errors.push(key);
          });
        }
      });
    },
    close: function close(remove) {
      this.modal = false;
      this.form = {
        file: null,
        ordem: null,
        url: {}
      };
      this.removeServer = remove;
      this.$refs.myDopzone.removeAllFiles(true);
      this.errors = [];
      this.$emit('updated');
    },
    confirmDelete: function confirmDelete(value) {
      this.$refs["delete"].open = true;
      this.selectedToDelete = value;
    },
    deleteFunction: function deleteFunction() {
      var _this3 = this;

      axios["delete"]("/dashboard/pages/homepage/removeSlider/" + this.selectedToDelete).then(function (response) {
        _this3.$refs["delete"].open = false;

        _this3.$refs.flash.handler(true, 'success', 'Eliminado com sucesso!');

        _this3.$emit('updated');
      })["catch"](function (error) {
        console.log(error);
      });
    }
  },
  watch: {
    data: {
      handler: function handler() {
        this.items = this.data ? this.data : [];
      },
      deep: true
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["data", "manufacturers"],
  components: {},
  data: function data() {
    var $vm = this;
    return {
      url: window.baseUrl,
      tab: 1,
      modal: false,
      text: null,
      action: null,
      positions: [{
        value: "left",
        text: "Esquerda"
      }, {
        value: "center",
        text: "Centro"
      }, {
        value: "right",
        text: "Direita"
      }],
      form: {
        title: {},
        type: null,
        posicao: [],
        files: [],
        url: {}
      },
      removeServer: true,
      dropzone: {
        url: "/dashboard/pages/homepage/uploadFileSlider",
        dictDefaultMessage: "Arraste ou selecione os ficheiros.",
        maxFilesize: 10,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: ".jpg,.jpeg,.png",
        headers: {
          "X-CSRF-TOKEN": document.head.querySelector("[name=csrf-token]").content
        },
        //Quando faz upload com sucesso
        success: function success(file, response) {
          $vm.form.files[0] = response;
          file.upload.name = response;
        },
        //Quando se clica em 'Remover'
        removedfile: function removedfile(file) {
          if ($vm.removeServer != false) {
            $vm.removeFile(file.upload.name, "homepage");
          }

          file.previewElement.remove();
        },
        //Quando excede o nº ficheiros
        maxfilesexceeded: function maxfilesexceeded(file) {
          file.previewElement.remove();
        },
        //Quando falha o tipo de ficheiro .PDF || ou Erro
        error: function error(file) {
          if (file.status == "error") {
            file.previewElement.remove();
          }
        }
      },
      dropzone2: {
        url: "/dashboard/pages/homepage/uploadFileSlider",
        dictDefaultMessage: "Arraste ou selecione os ficheiros.",
        maxFilesize: 10,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: ".jpg,.jpeg,.png",
        headers: {
          "X-CSRF-TOKEN": document.head.querySelector("[name=csrf-token]").content
        },
        //Quando faz upload com sucesso
        success: function success(file, response) {
          $vm.form.files[1] = response;
          file.upload.name = response;
        },
        //Quando se clica em 'Remover'
        removedfile: function removedfile(file) {
          if ($vm.removeServer != false) {
            $vm.removeFile(file.upload.name, "homepage");
          }

          file.previewElement.remove();
        },
        //Quando excede o nº ficheiros
        maxfilesexceeded: function maxfilesexceeded(file) {
          file.previewElement.remove();
        },
        //Quando falha o tipo de ficheiro .PDF || ou Erro
        error: function error(file) {
          if (file.status == "error") {
            file.previewElement.remove();
          }
        }
      },
      headers: [{
        text: "Imagem"
      }, {
        text: "Título"
      }, {
        text: "Designer"
      }, {
        text: "Ordem"
      }, {
        text: "Opções"
      }],
      items: [],
      selectedToDelete: null,
      formValid: false,
      rules: {
        required: [function (value) {
          return !!value || "Campo obrigatório.";
        }]
      },
      errors: []
    };
  },
  mounted: function mounted() {
    this.get();
  },
  methods: {
    openForm: function openForm(action, item, id) {
      this.action = action;
      this.modal = true;
      this.form = item ? item : {
        title: {},
        type: null,
        posicao: [],
        files: [],
        url: {}
      };
    },
    get: function get() {
      if (this.data) {
        this.items = this.data ? this.data : [];
      }
    },
    save: function save() {
      var _this = this;

      this.$refs.myForm.validate();
      var url = this.action == "store" ? this.action : this.action + "/" + this.form.id;
      axios.post("/dashboard/pages/homepage/slider_two/" + url, this.form).then(function (response) {
        _this.close(false);

        _this.$refs.flash.handler(true, "success", "Inserido com sucesso!");
      })["catch"](function (error) {
        if (error.response.status == 422) {
          _this.$refs.flash.handler(true, "error", "Campos em falta!");

          _this.errors = [];
          Object.keys(error.response.data.errors).map(function (key) {
            _this.errors.push(key);
          });
        }
      });
    },
    close: function close(remove) {
      this.modal = false;
      this.form = {
        title: {},
        type: null,
        posicao: [],
        files: [],
        url: {}
      };
      this.removeServer = remove;
      this.$refs.myDropzone.removeAllFiles(true);
      this.$refs.myDropzone2.removeAllFiles(true);
      this.$emit("updated");
    },
    removeFile: function removeFile(file, disk) {
      var _this2 = this;

      axios.post("/dashboard/pages/homepage/removeFileSlider", {
        file: file,
        disk: disk
      }).then(function (response) {
        _this2.$refs.flash.handler(true, "success", "Eliminado!");
      })["catch"](function (error) {
        console.log(error);
      });
    },
    confirmDelete: function confirmDelete(value) {
      this.$refs["delete"].open = true;
      this.selectedToDelete = value;
    },
    deleteFunction: function deleteFunction() {
      var _this3 = this;

      axios["delete"]("/dashboard/pages/homepage/removeSliderTwo/" + this.selectedToDelete).then(function (response) {
        _this3.$refs["delete"].open = false;

        _this3.$refs.flash.handler(true, "success", "Eliminado com sucesso!");

        _this3.$emit("updated");
      })["catch"](function (error) {
        console.log(error);
      });
    }
  },
  watch: {
    data: {
      handler: function handler() {
        this.items = this.data ? this.data : [];
      },
      deep: true
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['data'],
  data: function data() {
    return {
      loader: true,
      tab: 1,
      form: this.data ? this.data : {}
    };
  },
  mounted: function mounted() {
    this.loader = false;
  },
  methods: {
    save: function save() {
      var _this = this;

      console.log(this.form);
      axios.put("/dashboard/pages/homepage/text/update", this.form).then(function (response) {
        _this.$refs.flash.handler(true, 'success', 'Inserido com sucesso!');

        _this.$emit('updated');
      })["catch"](function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Video.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/components/Video.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue2-dropzone */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['data', 'manufacturers'],
  components: {
    Dropzone: vue2_dropzone__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  data: function data() {
    var $vm = this;
    return {
      url: window.baseUrl,
      action: null,
      modal: false,
      tab: 1,
      removeServer: true,
      dropzone: {
        url: "/dashboard/pages/homepage/uploadFileSlider",
        dictDefaultMessage: "Imagem (pré visualização)*.",
        maxFilesize: 30,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: ".jpg, .png",
        headers: {
          "X-CSRF-TOKEN": document.head.querySelector("[name=csrf-token]").content
        },
        success: function success(file, response) {
          $vm.form.image = response;
          file.upload.name = response;
        },
        removedfile: function removedfile(file) {
          if ($vm.removeServer != false) {
            $vm.removeFile(file.upload.name, 'homepage');
          }

          file.previewElement.remove();
        },
        maxfilesexceeded: function maxfilesexceeded(file) {
          file.previewElement.remove();
        },
        error: function error(file) {
          if (file.status == "error") {
            file.previewElement.remove();
          }
        }
      },
      dropzone2: {
        url: "/dashboard/pages/homepage/uploadFileSlider",
        dictDefaultMessage: "Vídeo.",
        maxFilesize: 30,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: ".mp4, .avi",
        headers: {
          "X-CSRF-TOKEN": document.head.querySelector("[name=csrf-token]").content
        },
        success: function success(file, response) {
          $vm.form.file = response;
          file.upload.name = response;
        },
        removedfile: function removedfile(file) {
          if ($vm.removeServer != false) {
            $vm.removeFile(file.upload.name, 'homepage');
          }

          file.previewElement.remove();
        },
        maxfilesexceeded: function maxfilesexceeded(file) {
          file.previewElement.remove();
        },
        error: function error(file) {
          if (file.status == "error") {
            file.previewElement.remove();
          }
        }
      },
      headers: [{
        text: "Vídeo"
      }, {
        text: "Título"
      }, {
        text: "Legenda"
      }, {
        text: "Designer"
      }, {
        text: "Ordem"
      }, {
        text: "Url"
      }, {
        text: "Opções"
      }],
      preview: {},
      items: [],
      form: {
        checkbox: false,
        title: {
          pt: null,
          en: null,
          fr: null,
          es: null,
          de: null
        },
        subtitle: {
          pt: null,
          en: null,
          fr: null,
          es: null,
          de: null
        },
        file: null,
        ordem: null,
        url: null
      },
      selectedToDelete: null,
      formData: new FormData(),
      formValid: false,
      rules: {
        required: [function (value) {
          return !!value || "Campo obrigatório.";
        }]
      },
      errors: []
    };
  },
  mounted: function mounted() {
    this.items = this.data ? this.data : [];
  },
  methods: {
    openForm: function openForm(action, item, id) {
      this.action = action;
      this.modal = true;
      this.form = item ? item : {
        title: {},
        subtitle: {},
        file: null,
        ordem: null,
        url: null
      };
    },
    removeFile: function removeFile(file, disk) {
      var _this = this;

      axios.post("/dashboard/pages/homepage/removeFileSlider", {
        file: file,
        disk: disk
      }).then(function (response) {
        _this.$refs.flash.handler(true, 'success', 'Eliminado!');
      })["catch"](function (error) {
        console.log(error);
      });
    },
    save: function save() {
      var _this2 = this;

      this.$refs.myForm.validate();
      var url = this.action == 'store' ? this.action : this.action + '/' + this.form.id;
      axios.post("/dashboard/pages/homepage/sliderVideo/" + url, this.form).then(function (response) {
        _this2.close(false);

        _this2.$refs.flash.handler(true, 'success', 'Inserido com sucesso!');
      })["catch"](function (error) {
        if (error.response.status == 422) {
          _this2.$refs.flash.handler(true, 'error', 'Campos em falta!');

          _this2.errors = [];
          Object.keys(error.response.data.errors).map(function (key) {
            _this2.errors.push(key);
          });
        }
      });
    },
    close: function close(remove) {
      this.modal = false;
      this.form = {
        checkbox: false,
        title: {
          pt: null,
          en: null,
          fr: null,
          es: null,
          de: null
        },
        subtitle: {
          pt: null,
          en: null,
          fr: null,
          es: null,
          de: null
        },
        file: null,
        ordem: null,
        url: null
      };
      this.removeServer = remove;
      this.$refs.myDropzone.removeAllFiles(true);
      this.$emit('updated');
    },
    confirmDelete: function confirmDelete(value) {
      this.$refs["delete"].open = true;
      this.selectedToDelete = value;
    },
    deleteFunction: function deleteFunction() {
      var _this3 = this;

      axios["delete"]("/dashboard/pages/homepage/removeSliderVideo/" + this.selectedToDelete).then(function (response) {
        _this3.$refs["delete"].open = false;

        _this3.$refs.flash.handler(true, 'success', 'Eliminado com sucesso!');

        _this3.$emit('updated');
      })["catch"](function (error) {
        console.log(error);
      });
    }
  },
  watch: {
    data: {
      handler: function handler() {
        this.items = this.data ? this.data : [];
      },
      deep: true
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/homepage.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/homepage.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_Slider_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/Slider.vue */ "./resources/js/views/dashboard/pages/components/Slider.vue");
/* harmony import */ var _components_Text_One_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/Text_One.vue */ "./resources/js/views/dashboard/pages/components/Text_One.vue");
/* harmony import */ var _components_Slider_Two_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/Slider_Two.vue */ "./resources/js/views/dashboard/pages/components/Slider_Two.vue");
/* harmony import */ var _components_Video_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/Video.vue */ "./resources/js/views/dashboard/pages/components/Video.vue");
/* harmony import */ var _components_Designers_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/Designers.vue */ "./resources/js/views/dashboard/pages/components/Designers.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  props: [],
  components: {
    Slider: _components_Slider_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    Text_One: _components_Text_One_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    Video: _components_Video_vue__WEBPACK_IMPORTED_MODULE_3__["default"],
    Slider_Two: _components_Slider_Two_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    Designers: _components_Designers_vue__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  data: function data() {
    return {
      loader: true,
      tab: 1,
      data: {},
      manufacturers: null
    };
  },
  mounted: function mounted() {
    this.get();
  },
  methods: {
    get: function get() {
      var _this = this;

      var get_homepage = axios.get('/dashboard/pages/homepage/get');
      var get_manufacturers = axios.get('/dashboard/manufacturers/get');
      axios.all([get_homepage, get_manufacturers]).then(function (response) {
        _this.data = response[0].data;
        _this.manufacturers = response[1].data;
        _this.loader = false;
      })["catch"](function (errors) {
        console.log(errors);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=template&id=048cc9d4&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=template&id=048cc9d4& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Flash", { ref: "flash" }),
      _vm._v(" "),
      _c("Delete", { ref: "delete", on: { onConfirm: _vm.deleteFunction } }),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "500px" },
          model: {
            value: _vm.modal,
            callback: function($$v) {
              _vm.modal = $$v
            },
            expression: "modal"
          }
        },
        [
          _c(
            "v-form",
            {
              ref: "myForm",
              model: {
                value: _vm.formValid,
                callback: function($$v) {
                  _vm.formValid = $$v
                },
                expression: "formValid"
              }
            },
            [
              _c(
                "v-card",
                [
                  _c("v-card-title", [_vm._v("Adicionar Designer")]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-12" },
                    [
                      _c("v-select", {
                        staticClass: "pa-0",
                        attrs: {
                          items: _vm.manufacturers,
                          "item-text": "name",
                          "item-id": "id",
                          label: "Designer",
                          rules: _vm.rules.required
                        },
                        model: {
                          value: _vm.form.manufacturer,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "manufacturer", $$v)
                          },
                          expression: "form.manufacturer"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-text-field", {
                        attrs: { label: "Ordem*", rules: _vm.rules.required },
                        model: {
                          value: _vm.form.ordem,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "ordem", $$v)
                          },
                          expression: "form.ordem"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card-actions",
                    [
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "success" },
                          on: {
                            click: function($event) {
                              $event.stopPropagation()
                              return _vm.save()
                            }
                          }
                        },
                        [_vm._v("Guardar")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "error" },
                          on: {
                            click: function($event) {
                              $event.stopPropagation()
                              return _vm.close(true)
                            }
                          }
                        },
                        [_vm._v("Fechar")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "col-sm-12" }, [
        _c("div", { staticClass: "row" }, [
          _c("h2", { staticClass: "mb-6 col-sm-6" }, [_vm._v("Designers")]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-sm-6" },
            [
              _c(
                "v-btn",
                {
                  staticClass: "ma-2",
                  attrs: { color: "primary" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      _vm.modal = true
                    }
                  }
                },
                [_vm._v("Adicionar")]
              )
            ],
            1
          )
        ])
      ]),
      _vm._v(" "),
      _c("v-data-table", {
        attrs: {
          headers: _vm.headers,
          items: _vm.items,
          "hide-default-footer": true
        },
        scopedSlots: _vm._u([
          {
            key: "item",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("tr", [
                  _c("td", [_vm._v(_vm._s(item.manufacturer))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.ordem))]),
                  _vm._v(" "),
                  _c("td", [
                    _c(
                      "button",
                      {
                        staticClass:
                          "mx-0 v-btn v-btn--flat v-btn--icon v-btn--round theme--light v-size--default",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            return _vm.confirmDelete(item.ordem)
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "v-btn__content" }, [
                          _c(
                            "i",
                            {
                              staticClass:
                                "v-icon notranslate error--text material-icons theme--light",
                              attrs: { "aria-hidden": "true" }
                            },
                            [_vm._v("close")]
                          )
                        ])
                      ]
                    )
                  ])
                ])
              ]
            }
          }
        ])
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=template&id=6c3f0f25&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=template&id=6c3f0f25& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Flash", { ref: "flash" }),
      _vm._v(" "),
      _c("Delete", { ref: "delete", on: { onConfirm: _vm.deleteFunction } }),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "500px" },
          model: {
            value: _vm.modal,
            callback: function($$v) {
              _vm.modal = $$v
            },
            expression: "modal"
          }
        },
        [
          _c(
            "v-form",
            {
              ref: "myForm",
              model: {
                value: _vm.formValid,
                callback: function($$v) {
                  _vm.formValid = $$v
                },
                expression: "formValid"
              }
            },
            [
              _c(
                "v-card",
                [
                  _c("v-card-title", [_vm._v("Adicionar Slider")]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-12" },
                    [
                      _c("dropzone", {
                        ref: "myDopzone",
                        staticClass: "mb-2",
                        class: { "error-input": _vm.errors.includes("file") },
                        attrs: { id: "myDopzone", options: _vm.dropzone },
                        on: {
                          click: function($event) {
                            _vm.form.type = 1
                          }
                        },
                        model: {
                          value: _vm.form.file,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "file", $$v)
                          },
                          expression: "form.file"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-select", {
                        staticClass: "mt-3 pt-3",
                        attrs: {
                          items: _vm.positions,
                          value: _vm.positions.value,
                          label: "Posição",
                          rules: _vm.rules.required
                        },
                        model: {
                          value: _vm.form.posicao,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "posicao", $$v)
                          },
                          expression: "form.posicao"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-text-field", {
                        attrs: { label: "Ordem*", rules: _vm.rules.required },
                        model: {
                          value: _vm.form.ordem,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "ordem", $$v)
                          },
                          expression: "form.ordem"
                        }
                      }),
                      _vm._v(" "),
                      _vm._l(this.$store.getters.languages, function(item) {
                        return _c("v-text-field", {
                          key: "item" + item.locale,
                          staticClass: "pb-3",
                          attrs: { label: "URL " + item.name },
                          model: {
                            value: _vm.form.url[item.locale],
                            callback: function($$v) {
                              _vm.$set(_vm.form.url, item.locale, $$v)
                            },
                            expression: "form.url[item.locale]"
                          }
                        })
                      })
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card-actions",
                    [
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "success" },
                          on: {
                            click: function($event) {
                              $event.stopPropagation()
                              return _vm.save()
                            }
                          }
                        },
                        [_vm._v("Guardar")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "error" },
                          on: {
                            click: function($event) {
                              $event.stopPropagation()
                              return _vm.close(true)
                            }
                          }
                        },
                        [_vm._v("Fechar")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "col-sm-12" }, [
        _c("div", { staticClass: "row" }, [
          _c("h2", { staticClass: "mb-6 col-sm-6" }, [_vm._v("Hero")]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-sm-6" },
            [
              _c(
                "v-btn",
                {
                  staticClass: "ma-2",
                  attrs: { color: "primary" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.openForm("store", null, null)
                    }
                  }
                },
                [_vm._v("Adicionar")]
              )
            ],
            1
          )
        ])
      ]),
      _vm._v(" "),
      _c("v-data-table", {
        attrs: {
          headers: _vm.headers,
          items: _vm.items,
          "hide-default-footer": true
        },
        scopedSlots: _vm._u([
          {
            key: "item",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("tr", [
                  _c("td", [
                    _c("img", {
                      staticClass: "pa-6 pl-0",
                      attrs: {
                        src: _vm.url + "/images/homepage/" + item.file,
                        height: "180px"
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.ordem))]),
                  _vm._v(" "),
                  _c("td", [
                    _c(
                      "button",
                      {
                        staticClass:
                          "mx-0 v-btn v-btn--flat v-btn--icon v-btn--round theme--light v-size--default",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.openForm("update", item, item.id)
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "v-btn__content" }, [
                          _c(
                            "i",
                            {
                              staticClass:
                                "v-icon notranslate primary--text material-icons theme--light",
                              attrs: { "aria-hidden": "true" }
                            },
                            [_vm._v("edit")]
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass:
                          "mx-0 v-btn v-btn--flat v-btn--icon v-btn--round theme--light v-size--default",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            return _vm.confirmDelete(item.id)
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "v-btn__content" }, [
                          _c(
                            "i",
                            {
                              staticClass:
                                "v-icon notranslate error--text material-icons theme--light",
                              attrs: { "aria-hidden": "true" }
                            },
                            [_vm._v("close")]
                          )
                        ])
                      ]
                    )
                  ])
                ])
              ]
            }
          }
        ])
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=template&id=2c8808dc&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=template&id=2c8808dc& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Flash", { ref: "flash" }),
      _vm._v(" "),
      _c("Delete", { ref: "delete", on: { onConfirm: _vm.deleteFunction } }),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "700px" },
          model: {
            value: _vm.modal,
            callback: function($$v) {
              _vm.modal = $$v
            },
            expression: "modal"
          }
        },
        [
          _c(
            "v-form",
            {
              ref: "myForm",
              model: {
                value: _vm.formValid,
                callback: function($$v) {
                  _vm.formValid = $$v
                },
                expression: "formValid"
              }
            },
            [
              _c(
                "v-card",
                [
                  _c("v-card-title", [_vm._v("Adicionar Slider")]),
                  _vm._v(" "),
                  _c(
                    "v-tabs",
                    {
                      staticClass:
                        "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    },
                    [
                      _c(
                        "v-tab",
                        {
                          on: {
                            click: function($event) {
                              _vm.tab = 1
                            }
                          }
                        },
                        [_vm._v("Textos")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-tab",
                        {
                          on: {
                            click: function($event) {
                              _vm.tab = 2
                            }
                          }
                        },
                        [_vm._v("Ordem")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-tab",
                        {
                          on: {
                            click: function($event) {
                              _vm.tab = 3
                            }
                          }
                        },
                        [_vm._v("Designer")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-tab",
                        {
                          on: {
                            click: function($event) {
                              _vm.tab = 4
                            }
                          }
                        },
                        [_vm._v("Imagens")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-tab",
                        {
                          on: {
                            click: function($event) {
                              _vm.tab = 5
                            }
                          }
                        },
                        [_vm._v("URL")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.tab == 1,
                          expression: "tab == 1"
                        }
                      ],
                      staticClass: "col-sm-12"
                    },
                    _vm._l(this.$store.getters.languages, function(item) {
                      return _c("v-text-field", {
                        key: "item" + item.locale,
                        staticClass: "pb-3",
                        attrs: { label: "Título " + item.name },
                        model: {
                          value: _vm.form.title[item.locale],
                          callback: function($$v) {
                            _vm.$set(_vm.form.title, item.locale, $$v)
                          },
                          expression: "form.title[item.locale]"
                        }
                      })
                    }),
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.tab == 2,
                          expression: "tab == 2"
                        }
                      ],
                      staticClass: "col-sm-12"
                    },
                    [
                      _c("v-text-field", {
                        staticClass: "pb-3",
                        attrs: { label: "Ordem", rules: _vm.rules.required },
                        model: {
                          value: _vm.form.ordem,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "ordem", $$v)
                          },
                          expression: "form.ordem"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.tab == 3,
                          expression: "tab == 3"
                        }
                      ],
                      staticClass: "col-sm-12"
                    },
                    [
                      _c("v-select", {
                        staticClass: "pa-0",
                        attrs: {
                          items: _vm.manufacturers,
                          "item-text": "name",
                          "item-id": "id",
                          label: "Designer",
                          rules: _vm.rules.required
                        },
                        model: {
                          value: _vm.form.manufacturer,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "manufacturer", $$v)
                          },
                          expression: "form.manufacturer"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.tab == 4,
                          expression: "tab == 4"
                        }
                      ],
                      staticClass: "col-sm-12"
                    },
                    [
                      _c("dropzone", {
                        ref: "myDropzone",
                        staticClass: "mb-2",
                        class: { "error-input": _vm.errors.includes("files") },
                        attrs: { id: "myDropzone", options: _vm.dropzone }
                      }),
                      _vm._v(" "),
                      _c("v-select", {
                        staticClass: "mt-3 pt-3",
                        attrs: {
                          items: _vm.positions,
                          value: _vm.positions.value,
                          label: "Posição",
                          rules: _vm.rules.required
                        },
                        model: {
                          value: _vm.form.posicao[0],
                          callback: function($$v) {
                            _vm.$set(_vm.form.posicao, 0, $$v)
                          },
                          expression: "form.posicao[0]"
                        }
                      }),
                      _vm._v(" "),
                      _c("dropzone", {
                        ref: "myDropzone2",
                        staticClass: "mb-2",
                        class: { "error-input": _vm.errors.includes("files") },
                        attrs: { id: "myDropzone2", options: _vm.dropzone2 }
                      }),
                      _vm._v(" "),
                      _c("v-select", {
                        staticClass: "mt-3 pt-3",
                        attrs: {
                          items: _vm.positions,
                          value: _vm.positions.value,
                          label: "Posição",
                          rules: _vm.rules.required
                        },
                        model: {
                          value: _vm.form.posicao[1],
                          callback: function($$v) {
                            _vm.$set(_vm.form.posicao, 1, $$v)
                          },
                          expression: "form.posicao[1]"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.tab == 5,
                          expression: "tab == 5"
                        }
                      ],
                      staticClass: "col-sm-12"
                    },
                    _vm._l(this.$store.getters.languages, function(item) {
                      return _c("v-text-field", {
                        key: "item" + item.locale,
                        staticClass: "pb-3",
                        attrs: { label: "URL " + item.name },
                        model: {
                          value: _vm.form.url[item.locale],
                          callback: function($$v) {
                            _vm.$set(_vm.form.url, item.locale, $$v)
                          },
                          expression: "form.url[item.locale]"
                        }
                      })
                    }),
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card-actions",
                    [
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "success" },
                          on: {
                            click: function($event) {
                              $event.stopPropagation()
                              return _vm.save()
                            }
                          }
                        },
                        [_vm._v("Guardar")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "error" },
                          on: {
                            click: function($event) {
                              $event.stopPropagation()
                              return _vm.close(true)
                            }
                          }
                        },
                        [_vm._v("Fechar")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "col-sm-12" }, [
        _c("div", { staticClass: "row" }, [
          _c("h2", { staticClass: "mb-6 col-sm-6" }, [_vm._v("Slider")]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-sm-6" },
            [
              _c(
                "v-btn",
                {
                  staticClass: "ma-2",
                  attrs: { color: "primary" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.openForm("store", null, null)
                    }
                  }
                },
                [_vm._v("Adicionar")]
              )
            ],
            1
          )
        ])
      ]),
      _vm._v(" "),
      _c("v-data-table", {
        attrs: {
          headers: _vm.headers,
          items: _vm.items,
          "hide-default-footer": true
        },
        scopedSlots: _vm._u([
          {
            key: "item",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("tr", [
                  _c("td", [
                    _c("img", {
                      staticClass: "pa-6 pl-0",
                      attrs: {
                        src: _vm.url + "/images/homepage/" + item.files[0],
                        height: "180px"
                      }
                    }),
                    _vm._v(" "),
                    _c("img", {
                      staticClass: "pa-6 pl-0",
                      attrs: {
                        src: _vm.url + "/images/homepage/" + item.files[1],
                        width: "140px"
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.title.pt))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.manufacturer))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.ordem))]),
                  _vm._v(" "),
                  _c("td", [
                    _c(
                      "button",
                      {
                        staticClass:
                          "\n              mx-0\n              v-btn v-btn--flat v-btn--icon v-btn--round\n              theme--light\n              v-size--default\n            ",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.openForm("update", item, item.id)
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "v-btn__content" }, [
                          _c(
                            "i",
                            {
                              staticClass:
                                "\n                  v-icon\n                  notranslate\n                  primary--text\n                  material-icons\n                  theme--light\n                ",
                              attrs: { "aria-hidden": "true" }
                            },
                            [_vm._v("edit")]
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass:
                          "\n              mx-0\n              v-btn v-btn--flat v-btn--icon v-btn--round\n              theme--light\n              v-size--default\n            ",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            return _vm.confirmDelete(item.id)
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "v-btn__content" }, [
                          _c(
                            "i",
                            {
                              staticClass:
                                "\n                  v-icon\n                  notranslate\n                  error--text\n                  material-icons\n                  theme--light\n                ",
                              attrs: { "aria-hidden": "true" }
                            },
                            [_vm._v("close")]
                          )
                        ])
                      ]
                    )
                  ])
                ])
              ]
            }
          }
        ])
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=template&id=b7bab410&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=template&id=b7bab410& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.loader == false
    ? _c(
        "div",
        [
          _c("Flash", { ref: "flash" }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-sm-12" },
            [
              _c("h2", [_vm._v("Texto")]),
              _vm._v(" "),
              _vm._l(this.$store.getters.languages, function(item) {
                return _c("v-text-field", {
                  key: "item" + item.locale,
                  attrs: { label: "Texto " + item.name },
                  model: {
                    value: _vm.form[item.locale],
                    callback: function($$v) {
                      _vm.$set(_vm.form, item.locale, $$v)
                    },
                    expression: "form[item.locale]"
                  }
                })
              }),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  attrs: { color: "success" },
                  on: {
                    click: function($event) {
                      $event.stopPropagation()
                      return _vm.save()
                    }
                  }
                },
                [_vm._v("Guardar")]
              )
            ],
            2
          )
        ],
        1
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Video.vue?vue&type=template&id=c9dac032&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/components/Video.vue?vue&type=template&id=c9dac032& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Flash", { ref: "flash" }),
      _vm._v(" "),
      _c("Delete", { ref: "delete", on: { onConfirm: _vm.deleteFunction } }),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "700px" },
          model: {
            value: _vm.modal,
            callback: function($$v) {
              _vm.modal = $$v
            },
            expression: "modal"
          }
        },
        [
          _c(
            "v-form",
            {
              ref: "myForm",
              model: {
                value: _vm.formValid,
                callback: function($$v) {
                  _vm.formValid = $$v
                },
                expression: "formValid"
              }
            },
            [
              _c(
                "v-card",
                [
                  _c("v-card-title", [_vm._v("Adicionar Slider Vídeo")]),
                  _vm._v(" "),
                  _c(
                    "v-tabs",
                    {
                      staticClass:
                        "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    },
                    [
                      _c(
                        "v-tab",
                        {
                          on: {
                            click: function($event) {
                              _vm.tab = 1
                            }
                          }
                        },
                        [_vm._v("Textos")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-tab",
                        {
                          on: {
                            click: function($event) {
                              _vm.tab = 2
                            }
                          }
                        },
                        [_vm._v("Ordem")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-tab",
                        {
                          on: {
                            click: function($event) {
                              _vm.tab = 3
                            }
                          }
                        },
                        [_vm._v("Designer")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-tab",
                        {
                          on: {
                            click: function($event) {
                              _vm.tab = 4
                            }
                          }
                        },
                        [_vm._v("Media")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.tab == 1,
                          expression: "tab == 1"
                        }
                      ],
                      staticClass: "col-sm-12"
                    },
                    _vm._l(this.$store.getters.languages, function(item) {
                      return _c(
                        "app-card",
                        { key: "item" + item.locale, staticClass: "pb-6" },
                        [
                          _c("label", [_vm._v(_vm._s(item.name))]),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: { label: "Título " + item.name },
                            model: {
                              value: _vm.form.title[item.locale],
                              callback: function($$v) {
                                _vm.$set(_vm.form.title, item.locale, $$v)
                              },
                              expression: "form.title[item.locale]"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: { label: "Legenda " + item.name },
                            model: {
                              value: _vm.form.subtitle[item.locale],
                              callback: function($$v) {
                                _vm.$set(_vm.form.subtitle, item.locale, $$v)
                              },
                              expression: "form.subtitle[item.locale]"
                            }
                          })
                        ],
                        1
                      )
                    }),
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.tab == 2,
                          expression: "tab == 2"
                        }
                      ],
                      staticClass: "col-sm-12"
                    },
                    [
                      _c("v-text-field", {
                        attrs: { label: "Ordem*", rules: _vm.rules.required },
                        model: {
                          value: _vm.form.ordem,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "ordem", $$v)
                          },
                          expression: "form.ordem"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.tab == 3,
                          expression: "tab == 3"
                        }
                      ],
                      staticClass: "col-sm-12"
                    },
                    [
                      _c("v-select", {
                        staticClass: "pa-0",
                        attrs: {
                          items: _vm.manufacturers,
                          "item-text": "name",
                          "item-id": "id",
                          label: "Designer*",
                          rules: _vm.rules.required
                        },
                        model: {
                          value: _vm.form.manufacturer,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "manufacturer", $$v)
                          },
                          expression: "form.manufacturer"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.tab == 4,
                          expression: "tab == 4"
                        }
                      ],
                      staticClass: "col-sm-12"
                    },
                    [
                      _c("v-checkbox", {
                        attrs: { color: "primary", label: "Upload" },
                        model: {
                          value: _vm.form.checkbox,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "checkbox", $$v)
                          },
                          expression: "form.checkbox"
                        }
                      }),
                      _vm._v(" "),
                      _c("dropzone", {
                        ref: "myDopzone",
                        staticClass: "mb-2",
                        class: { "error-input": _vm.errors.includes("file") },
                        attrs: { id: "myDopzone", options: _vm.dropzone },
                        model: {
                          value: _vm.form.file,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "file", $$v)
                          },
                          expression: "form.file"
                        }
                      }),
                      _vm._v(" "),
                      _vm.form.checkbox
                        ? _c("dropzone", {
                            ref: "myDropzone",
                            staticClass: "mb-2",
                            class: {
                              "error-input": _vm.errors.includes("file")
                            },
                            attrs: {
                              id: "myDropzone2",
                              options: _vm.dropzone2
                            },
                            model: {
                              value: _vm.form.file,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "file", $$v)
                              },
                              expression: "form.file"
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.form.checkbox
                        ? _c("v-text-field", {
                            attrs: { label: "Url*" },
                            model: {
                              value: _vm.form.url,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "url", $$v)
                              },
                              expression: "form.url"
                            }
                          })
                        : _vm._e()
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card-actions",
                    [
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "success" },
                          on: {
                            click: function($event) {
                              $event.stopPropagation()
                              return _vm.save()
                            }
                          }
                        },
                        [_vm._v("Guardar")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "error" },
                          on: {
                            click: function($event) {
                              $event.stopPropagation()
                              return _vm.close(true)
                            }
                          }
                        },
                        [_vm._v("Fechar")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "col-sm-12" }, [
        _c("div", { staticClass: "row" }, [
          _c("h2", { staticClass: "mb-6 col-sm-6" }, [_vm._v("Slider Vídeo")]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-sm-6" },
            [
              _c(
                "v-btn",
                {
                  staticClass: "ma-2",
                  attrs: { color: "primary" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.openForm("store", null, null)
                    }
                  }
                },
                [_vm._v("Adicionar")]
              )
            ],
            1
          )
        ])
      ]),
      _vm._v(" "),
      _c("v-data-table", {
        attrs: {
          headers: _vm.headers,
          items: _vm.items,
          "hide-default-footer": true
        },
        scopedSlots: _vm._u([
          {
            key: "item",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("tr", [
                  _c("td", [
                    _c("img", {
                      attrs: {
                        src: _vm.url + "/images/homepage/" + item.image,
                        width: "180px;"
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.title.pt))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.subtitle.pt))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.manufacturer))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.ordem))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.url))]),
                  _vm._v(" "),
                  _c("td", [
                    _c(
                      "button",
                      {
                        staticClass:
                          "mx-0 v-btn v-btn--flat v-btn--icon v-btn--round theme--light v-size--default",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.openForm("update", item, item.id)
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "v-btn__content" }, [
                          _c(
                            "i",
                            {
                              staticClass:
                                "v-icon notranslate primary--text material-icons theme--light",
                              attrs: { "aria-hidden": "true" }
                            },
                            [_vm._v("edit")]
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass:
                          "mx-0 v-btn v-btn--flat v-btn--icon v-btn--round theme--light v-size--default",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            return _vm.confirmDelete(item.id)
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "v-btn__content" }, [
                          _c(
                            "i",
                            {
                              staticClass:
                                "v-icon notranslate error--text material-icons theme--light",
                              attrs: { "aria-hidden": "true" }
                            },
                            [_vm._v("close")]
                          )
                        ])
                      ]
                    )
                  ])
                ])
              ]
            }
          }
        ])
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/homepage.vue?vue&type=template&id=6f00e2b3&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/homepage.vue?vue&type=template&id=6f00e2b3& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.loader == false
    ? _c(
        "div",
        [
          _c("app-card", { staticClass: "col-md-12" }, [
            _c("h3", [_vm._v("Homepage")])
          ]),
          _vm._v(" "),
          _c(
            "v-tabs",
            { staticClass: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" },
            [
              _c(
                "v-tab",
                {
                  on: {
                    click: function($event) {
                      _vm.tab = 1
                    }
                  }
                },
                [_vm._v("Hero")]
              ),
              _vm._v(" "),
              _c(
                "v-tab",
                {
                  on: {
                    click: function($event) {
                      _vm.tab = 2
                    }
                  }
                },
                [_vm._v("Texto")]
              ),
              _vm._v(" "),
              _c(
                "v-tab",
                {
                  on: {
                    click: function($event) {
                      _vm.tab = 3
                    }
                  }
                },
                [_vm._v("Slider")]
              ),
              _vm._v(" "),
              _c(
                "v-tab",
                {
                  on: {
                    click: function($event) {
                      _vm.tab = 4
                    }
                  }
                },
                [_vm._v("Vídeo")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "app-card",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.tab == 1,
                  expression: "tab == 1"
                }
              ],
              staticClass: "col-md-12"
            },
            [
              _c("Slider", {
                attrs: { data: _vm.data.slider },
                on: {
                  updated: function($event) {
                    return _vm.get()
                  }
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "app-card",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.tab == 2,
                  expression: "tab == 2"
                }
              ],
              staticClass: "col-md-12"
            },
            [
              _c("Text_One", {
                attrs: { data: _vm.data.text_one },
                on: {
                  updated: function($event) {
                    return _vm.get()
                  }
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "app-card",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.tab == 3,
                  expression: "tab == 3"
                }
              ],
              staticClass: "col-md-12"
            },
            [
              _c("Slider_Two", {
                attrs: {
                  data: _vm.data.slider_two,
                  manufacturers: _vm.manufacturers
                },
                on: {
                  updated: function($event) {
                    return _vm.get()
                  }
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "app-card",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.tab == 4,
                  expression: "tab == 4"
                }
              ],
              staticClass: "col-md-12"
            },
            [
              _c("Video", {
                attrs: {
                  data: _vm.data.video,
                  manufacturers: _vm.manufacturers
                },
                on: {
                  updated: function($event) {
                    return _vm.get()
                  }
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "app-card",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.tab == 5,
                  expression: "tab == 5"
                }
              ],
              staticClass: "col-md-12"
            },
            [
              _c("Designers", {
                attrs: {
                  data: _vm.data.slider_three,
                  manufacturers: _vm.manufacturers
                },
                on: {
                  updated: function($event) {
                    return _vm.get()
                  }
                }
              })
            ],
            1
          )
        ],
        1
      )
    : _c("div", [_c("Progress")], 1)
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Designers.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Designers.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Designers_vue_vue_type_template_id_048cc9d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Designers.vue?vue&type=template&id=048cc9d4& */ "./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=template&id=048cc9d4&");
/* harmony import */ var _Designers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Designers.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Designers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Designers_vue_vue_type_template_id_048cc9d4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Designers_vue_vue_type_template_id_048cc9d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/pages/components/Designers.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Designers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Designers.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Designers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=template&id=048cc9d4&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=template&id=048cc9d4& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Designers_vue_vue_type_template_id_048cc9d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Designers.vue?vue&type=template&id=048cc9d4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Designers.vue?vue&type=template&id=048cc9d4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Designers_vue_vue_type_template_id_048cc9d4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Designers_vue_vue_type_template_id_048cc9d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Slider.vue":
/*!******************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Slider.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Slider_vue_vue_type_template_id_6c3f0f25___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Slider.vue?vue&type=template&id=6c3f0f25& */ "./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=template&id=6c3f0f25&");
/* harmony import */ var _Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Slider.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Slider_vue_vue_type_template_id_6c3f0f25___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Slider_vue_vue_type_template_id_6c3f0f25___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/pages/components/Slider.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Slider.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=template&id=6c3f0f25&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=template&id=6c3f0f25& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_template_id_6c3f0f25___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Slider.vue?vue&type=template&id=6c3f0f25& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Slider.vue?vue&type=template&id=6c3f0f25&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_template_id_6c3f0f25___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_template_id_6c3f0f25___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Slider_Two.vue":
/*!**********************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Slider_Two.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Slider_Two_vue_vue_type_template_id_2c8808dc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Slider_Two.vue?vue&type=template&id=2c8808dc& */ "./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=template&id=2c8808dc&");
/* harmony import */ var _Slider_Two_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Slider_Two.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Slider_Two_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Slider_Two_vue_vue_type_template_id_2c8808dc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Slider_Two_vue_vue_type_template_id_2c8808dc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/pages/components/Slider_Two.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_Two_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Slider_Two.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_Two_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=template&id=2c8808dc&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=template&id=2c8808dc& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_Two_vue_vue_type_template_id_2c8808dc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Slider_Two.vue?vue&type=template&id=2c8808dc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Slider_Two.vue?vue&type=template&id=2c8808dc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_Two_vue_vue_type_template_id_2c8808dc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_Two_vue_vue_type_template_id_2c8808dc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Text_One.vue":
/*!********************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Text_One.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Text_One_vue_vue_type_template_id_b7bab410___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Text_One.vue?vue&type=template&id=b7bab410& */ "./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=template&id=b7bab410&");
/* harmony import */ var _Text_One_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Text_One.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Text_One_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Text_One_vue_vue_type_template_id_b7bab410___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Text_One_vue_vue_type_template_id_b7bab410___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/pages/components/Text_One.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Text_One_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Text_One.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Text_One_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=template&id=b7bab410&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=template&id=b7bab410& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Text_One_vue_vue_type_template_id_b7bab410___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Text_One.vue?vue&type=template&id=b7bab410& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Text_One.vue?vue&type=template&id=b7bab410&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Text_One_vue_vue_type_template_id_b7bab410___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Text_One_vue_vue_type_template_id_b7bab410___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Video.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Video.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Video_vue_vue_type_template_id_c9dac032___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Video.vue?vue&type=template&id=c9dac032& */ "./resources/js/views/dashboard/pages/components/Video.vue?vue&type=template&id=c9dac032&");
/* harmony import */ var _Video_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Video.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/pages/components/Video.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Video_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Video_vue_vue_type_template_id_c9dac032___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Video_vue_vue_type_template_id_c9dac032___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/pages/components/Video.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Video.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Video.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Video_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Video.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Video.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Video_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/pages/components/Video.vue?vue&type=template&id=c9dac032&":
/*!************************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/components/Video.vue?vue&type=template&id=c9dac032& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Video_vue_vue_type_template_id_c9dac032___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Video.vue?vue&type=template&id=c9dac032& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/components/Video.vue?vue&type=template&id=c9dac032&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Video_vue_vue_type_template_id_c9dac032___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Video_vue_vue_type_template_id_c9dac032___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/dashboard/pages/homepage.vue":
/*!*********************************************************!*\
  !*** ./resources/js/views/dashboard/pages/homepage.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _homepage_vue_vue_type_template_id_6f00e2b3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./homepage.vue?vue&type=template&id=6f00e2b3& */ "./resources/js/views/dashboard/pages/homepage.vue?vue&type=template&id=6f00e2b3&");
/* harmony import */ var _homepage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./homepage.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/pages/homepage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _homepage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _homepage_vue_vue_type_template_id_6f00e2b3___WEBPACK_IMPORTED_MODULE_0__["render"],
  _homepage_vue_vue_type_template_id_6f00e2b3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/pages/homepage.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/pages/homepage.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/homepage.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_homepage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./homepage.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/homepage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_homepage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/pages/homepage.vue?vue&type=template&id=6f00e2b3&":
/*!****************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/homepage.vue?vue&type=template&id=6f00e2b3& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_homepage_vue_vue_type_template_id_6f00e2b3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./homepage.vue?vue&type=template&id=6f00e2b3& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/homepage.vue?vue&type=template&id=6f00e2b3&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_homepage_vue_vue_type_template_id_6f00e2b3___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_homepage_vue_vue_type_template_id_6f00e2b3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);