(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[11],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/designers/form.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/designers/form.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['id'],
  components: {},
  data: function data() {
    var $vm = this;
    return {
      url: window.baseUrl,
      loader: true,
      tab: 1,
      form: {},
      formData: new FormData(),
      preview: {},
      errors: [],
      positions: [{
        value: 'left',
        text: 'Esquerda'
      }, {
        value: 'center',
        text: 'Centro'
      }, {
        value: 'right',
        text: 'Direita'
      }],
      rules: {
        required: [function (value) {
          return !!value || "Campo obrigatório.";
        }]
      },
      dropzone1: {
        url: "/dashboard/manufacturers/upload/image/" + this.id,
        dictDefaultMessage: "Imagem (pré visualização)*.",
        maxFilesize: 30,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: ".jpg, .png, .webp",
        headers: {
          "X-CSRF-TOKEN": document.head.querySelector("[name=csrf-token]").content
        },
        init: function init() {
          if ($vm.form.info[this.element.id]) {
            var cloneImg = Object.assign($vm.form.info[this.element.id]);
            this.emit("addedfile", cloneImg);
            this.emit("thumbnail", cloneImg, $vm.url + '/images/manufacturers/' + $vm.id + '/' + $vm.form.info[this.element.id]);
            this.emit("complete", cloneImg);
            $vm.form[this.element.id] = $vm.form.info[this.element.id];
          }
        },
        success: function success(file, response) {
          $vm.form[this.element.id] = response;
          file.upload.name = response;
        },
        //Quando se clica em 'Remover'
        removedfile: function removedfile(file) {
          $vm.form[this.element.id] = null;
          file.previewElement.remove();
        },
        maxfilesexceeded: function maxfilesexceeded(file) {
          file.previewElement.remove();
        },
        error: function error(file) {
          if (file.status == "error") {
            file.previewElement.remove();
          }
        }
      }
    };
  },
  mounted: function mounted() {
    this.get();
  },
  methods: {
    removeImage: function removeImage(value) {
      var _this = this;

      axios.put('/dashboard/manufacturers/removeImage/' + value + '/' + this.id).then(function (response) {
        _this.get();

        _this.$forceUpdate();
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    get: function get() {
      var _this2 = this;

      axios.get('/dashboard/manufacturers/fetch/' + this.id).then(function (response) {
        if (response.data.info) {
          _this2.form = Object.assign(response.data, response.data.info);
          _this2.form.texto = response.data.info.texto ? response.data.info.texto : {};

          if (response.data.info.posicao) {
            _this2.form.posicao = JSON.parse(response.data.info.posicao);
          } else {
            _this2.form.posicao = [];
          }
        }

        _this2.loader = false;
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    previewImages: function previewImages() {
      if (this.form.imagem_perfil) {
        this.preview.imagem_perfil = URL.createObjectURL(this.form.imagem_perfil);
      }

      if (this.form.imagem_produto) {
        this.preview.imagem_produto = URL.createObjectURL(this.form.imagem_produto);
      }

      if (this.form.imagem_assinatura) {
        this.preview.imagem_assinatura = URL.createObjectURL(this.form.imagem_assinatura);
      }
    },
    save: function save() {
      var _this3 = this;

      axios.post('/dashboard/manufacturers/update/' + this.id, this.form).then(function (response) {
        _this3.loader = false;

        _this3.$refs.flash.handler(true, 'success', 'Guardado com sucesso!');

        setTimeout(function () {
          return window.location.href = "/dashboard/designers";
        }, 1500);
      })["catch"](function (error) {
        if (error.response.status == 422) {
          _this3.errors = [];
          Object.keys(error.response.data.errors).map(function (key) {
            _this3.errors.push(key);
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/designers/form.vue?vue&type=template&id=d65a3936&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/designers/form.vue?vue&type=template&id=d65a3936& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.loader == false
    ? _c(
        "div",
        [
          _c("Flash", { ref: "flash" }),
          _vm._v(" "),
          _c("app-card", { staticClass: "col-sm-12" }, [
            _c("div", [_c("h3", [_vm._v(_vm._s(_vm.form.name))])]),
            _vm._v(" "),
            _c(
              "div",
              [
                _c(
                  "v-tabs",
                  { staticClass: "col-sm-12" },
                  [
                    _c(
                      "v-tab",
                      {
                        on: {
                          click: function($event) {
                            _vm.tab = 1
                          }
                        }
                      },
                      [_vm._v("Textos")]
                    ),
                    _vm._v(" "),
                    _c(
                      "v-tab",
                      {
                        on: {
                          click: function($event) {
                            _vm.tab = 2
                          }
                        }
                      },
                      [_vm._v("Imagens")]
                    ),
                    _vm._v(" "),
                    _c(
                      "v-tab",
                      {
                        on: {
                          click: function($event) {
                            _vm.tab = 3
                          }
                        }
                      },
                      [_vm._v("Ordem")]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.tab == 1,
                        expression: "tab == 1"
                      }
                    ],
                    staticClass: "col-sm-12"
                  },
                  _vm._l(this.$store.getters.languages, function(item) {
                    return _c(
                      "div",
                      { key: "item" + item.locale, staticClass: "pb-3 mb-3" },
                      [
                        _c("v-textarea", {
                          attrs: { label: "Texto " + item.name },
                          model: {
                            value: _vm.form.texto[item.locale],
                            callback: function($$v) {
                              _vm.$set(_vm.form.texto, item.locale, $$v)
                            },
                            expression: "form.texto[item.locale]"
                          }
                        })
                      ],
                      1
                    )
                  }),
                  0
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.tab == 2,
                        expression: "tab == 2"
                      }
                    ],
                    staticClass: "col-sm-12"
                  },
                  [
                    _c(
                      "div",
                      { staticClass: "pb-3 mb-3" },
                      [
                        _c("label", [_vm._v("Imagem de perfil")]),
                        _vm._v(" "),
                        _c("dropzone", {
                          ref: "myDopzone",
                          class: { "error-input": _vm.errors.includes("file") },
                          attrs: { id: "imagem_perfil", options: _vm.dropzone1 }
                        }),
                        _vm._v(" "),
                        _c("v-select", {
                          staticClass: "mt-3 pt-3",
                          attrs: {
                            items: _vm.positions,
                            value: _vm.positions.value,
                            label: "Posição",
                            rules: _vm.rules.required
                          },
                          model: {
                            value: _vm.form.posicao[0],
                            callback: function($$v) {
                              _vm.$set(_vm.form.posicao, 0, $$v)
                            },
                            expression: "form.posicao[0]"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "pb-3 mb-3" },
                      [
                        _c("label", [_vm._v("Imagem de produto")]),
                        _vm._v(" "),
                        _c("dropzone", {
                          ref: "myDopzone",
                          class: { "error-input": _vm.errors.includes("file") },
                          attrs: {
                            id: "imagem_produto",
                            options: _vm.dropzone1
                          }
                        }),
                        _vm._v(" "),
                        _c("v-select", {
                          staticClass: "mt-3 pt-3",
                          attrs: {
                            items: _vm.positions,
                            value: _vm.positions.value,
                            label: "Posição",
                            rules: _vm.rules.required
                          },
                          model: {
                            value: _vm.form.posicao[1],
                            callback: function($$v) {
                              _vm.$set(_vm.form.posicao, 1, $$v)
                            },
                            expression: "form.posicao[1]"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "pb-3 mb-3" },
                      [
                        _c("label", [_vm._v("Imagem de assinatura")]),
                        _vm._v(" "),
                        _c("dropzone", {
                          ref: "myDopzone",
                          class: { "error-input": _vm.errors.includes("file") },
                          attrs: {
                            id: "imagem_assinatura",
                            options: _vm.dropzone1
                          }
                        })
                      ],
                      1
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.tab == 3,
                        expression: "tab == 3"
                      }
                    ],
                    staticClass: "col-sm-12"
                  },
                  [
                    _c("v-text-field", {
                      attrs: { label: "Ordem" },
                      model: {
                        value: _vm.form.order,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "order", $$v)
                        },
                        expression: "form.order"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-sm-12 pl-0 ml-0 pt-3" },
                  [
                    _c(
                      "v-btn",
                      {
                        attrs: { color: "success" },
                        on: {
                          click: function($event) {
                            $event.stopPropagation()
                            return _vm.save()
                          }
                        }
                      },
                      [_vm._v("Guardar")]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        ],
        1
      )
    : _c("div", [_c("Progress")], 1)
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/dashboard/designers/form.vue":
/*!*********************************************************!*\
  !*** ./resources/js/views/dashboard/designers/form.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _form_vue_vue_type_template_id_d65a3936___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./form.vue?vue&type=template&id=d65a3936& */ "./resources/js/views/dashboard/designers/form.vue?vue&type=template&id=d65a3936&");
/* harmony import */ var _form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/designers/form.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _form_vue_vue_type_template_id_d65a3936___WEBPACK_IMPORTED_MODULE_0__["render"],
  _form_vue_vue_type_template_id_d65a3936___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/designers/form.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/designers/form.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/views/dashboard/designers/form.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./form.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/designers/form.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/designers/form.vue?vue&type=template&id=d65a3936&":
/*!****************************************************************************************!*\
  !*** ./resources/js/views/dashboard/designers/form.vue?vue&type=template&id=d65a3936& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_d65a3936___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./form.vue?vue&type=template&id=d65a3936& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/designers/form.vue?vue&type=template&id=d65a3936&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_d65a3936___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_d65a3936___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);