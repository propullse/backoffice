(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[15],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/categories/form.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/categories/form.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['page', 'page_id'],
  components: {},
  data: function data() {
    return {
      url: window.baseUrl,
      loader: true,
      tab: 1,
      action: null,
      form: {},
      formData: new FormData(),
      preview: {},
      errors: [],
      positions: [{
        value: 'left',
        text: 'Esquerda'
      }, {
        value: 'center',
        text: 'Centro'
      }, {
        value: 'right',
        text: 'Direita'
      }],
      formValid: false,
      rules: {
        required: [function (value) {
          return !!value || "Campo obrigatório.";
        }]
      }
    };
  },
  mounted: function mounted() {
    this.get();
  },
  methods: {
    get: function get() {
      var _this = this;

      axios.get('/dashboard/pages/fetch/' + this.page).then(function (response) {
        if (response.data) {
          _this.form = JSON.parse(response.data.content);
          _this.preview.imagem_esquerda = _this.url + '/categories/' + _this.form.imagem_esquerda;
          _this.preview.imagem_direita = _this.url + '/categories/' + _this.form.imagem_direita;
          _this.form = {
            id_category: _this.page_id,
            imagem_direita: null,
            imagem_esquerda: null,
            posicao: JSON.parse(_this.form.posicao)
          };
          _this.action = 'update';
        } else {
          _this.form = {
            id_category: _this.page_id,
            imagem_direita: null,
            imagem_esquerda: null,
            posicao: []
          };
          _this.action = 'store';
        }

        _this.form.action = _this.action;
        _this.form.url = _this.page;
        _this.form.is_category = true;
        _this.loader = false;
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    previewImages: function previewImages() {
      if (this.form.imagem_direita) {
        this.preview.imagem_direita = URL.createObjectURL(this.form.imagem_direita);
      }

      if (this.form.imagem_esquerda) {
        this.preview.imagem_esquerda = URL.createObjectURL(this.form.imagem_esquerda);
      }
    },
    save: function save() {
      var _this2 = this;

      if (this.action == 'store') {
        this.$refs.myForm.validate();
      }

      Object.keys(this.form).map(function (key, value) {
        if (_this2.form[key]) {
          if (key == 'texto' || key == 'posicao') {
            _this2.formData.append(key, JSON.stringify(_this2.form[key]));
          } else {
            _this2.formData.append(key, _this2.form[key]);
          }
        }
      });
      axios.post('/dashboard/pages/store/' + this.page, this.formData).then(function (response) {
        _this2.loader = false;

        _this2.$refs.flash.handler(true, 'success', 'Guardado com sucesso!'); //setTimeout(() => window.location.href = "/dashboard/designers", 1500);

      })["catch"](function (error) {
        console.log(error);

        if (error.response.status == 422) {
          _this2.errors = [];
          Object.keys(error.response.data.errors).map(function (key) {
            _this2.errors.push(key);
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/categories/form.vue?vue&type=template&id=59c817bc&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/categories/form.vue?vue&type=template&id=59c817bc& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.loader == false
    ? _c(
        "div",
        [
          _c("Flash", { ref: "flash" }),
          _vm._v(" "),
          _c("app-card", { staticClass: "col-sm-12" }, [
            _c("div", [_c("h3", [_vm._v(_vm._s(_vm.page))])]),
            _vm._v(" "),
            _c(
              "div",
              [
                _c(
                  "v-tabs",
                  { staticClass: "col-sm-12" },
                  [
                    _c(
                      "v-tab",
                      {
                        on: {
                          click: function($event) {
                            _vm.tab = 1
                          }
                        }
                      },
                      [_vm._v("Imagens")]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.tab == 1,
                        expression: "tab == 1"
                      }
                    ]
                  },
                  [
                    _c(
                      "v-form",
                      {
                        ref: "myForm",
                        model: {
                          value: _vm.formValid,
                          callback: function($$v) {
                            _vm.formValid = $$v
                          },
                          expression: "formValid"
                        }
                      },
                      [
                        _c(
                          "app-card",
                          { staticClass: "pb-3 mb-3" },
                          [
                            _c("v-file-input", {
                              staticClass: "mb-3",
                              attrs: {
                                label: "Imagem (esquerda)",
                                "prepend-icon": "",
                                dense: "",
                                counter: "",
                                "show-size": "",
                                rules: _vm.rules.required
                              },
                              on: { change: _vm.previewImages },
                              model: {
                                value: _vm.form.imagem_esquerda,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "imagem_esquerda", $$v)
                                },
                                expression: "form.imagem_esquerda"
                              }
                            }),
                            _vm._v(" "),
                            _c("v-select", {
                              staticClass: "mt-3 pt-3",
                              attrs: {
                                items: _vm.positions,
                                value: _vm.positions.value,
                                label: "Posição"
                              },
                              model: {
                                value: _vm.form.posicao[0],
                                callback: function($$v) {
                                  _vm.$set(_vm.form.posicao, 0, $$v)
                                },
                                expression: "form.posicao[0]"
                              }
                            }),
                            _vm._v(" "),
                            _c("v-img", {
                              staticClass: "mb-6",
                              attrs: {
                                src: _vm.preview.imagem_esquerda,
                                width: "200px"
                              }
                            }),
                            _vm._v(" "),
                            _vm.form.imagem_esquerda &&
                            !_vm.preview.imagem_esquerda
                              ? _c("v-img", {
                                  staticClass: "mb-6",
                                  attrs: {
                                    src:
                                      _vm.url +
                                      "/images/manufacturers/" +
                                      _vm.form.id_manufacturer +
                                      "/" +
                                      _vm.form.imagem_esquerda,
                                    width: "200px"
                                  }
                                })
                              : _vm._e()
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "app-card",
                          { staticClass: "pb-3 mb-3" },
                          [
                            _c("v-file-input", {
                              staticClass: "mb-3",
                              attrs: {
                                label: "Imagem (direita)",
                                "prepend-icon": "",
                                dense: "",
                                counter: "",
                                "show-size": "",
                                rules: _vm.rules.required
                              },
                              on: { change: _vm.previewImages },
                              model: {
                                value: _vm.form.imagem_direita,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "imagem_direita", $$v)
                                },
                                expression: "form.imagem_direita"
                              }
                            }),
                            _vm._v(" "),
                            _c("v-select", {
                              staticClass: "mt-3 pt-3",
                              attrs: {
                                items: _vm.positions,
                                value: _vm.positions.value,
                                label: "Posição"
                              },
                              model: {
                                value: _vm.form.posicao[1],
                                callback: function($$v) {
                                  _vm.$set(_vm.form.posicao, 1, $$v)
                                },
                                expression: "form.posicao[1]"
                              }
                            }),
                            _vm._v(" "),
                            _c("v-img", {
                              staticClass: "mb-6",
                              attrs: {
                                src: _vm.preview.imagem_direita,
                                width: "200px"
                              }
                            }),
                            _vm._v(" "),
                            _vm.form.imagem_direita &&
                            !_vm.preview.imagem_direita
                              ? _c("v-img", {
                                  staticClass: "mb-6",
                                  attrs: {
                                    src:
                                      _vm.url +
                                      "/images/manufacturers/" +
                                      _vm.form.id_manufacturer +
                                      "/" +
                                      _vm.form.imagem_direita,
                                    width: "200px"
                                  }
                                })
                              : _vm._e()
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-sm-12 pl-0 ml-0 pt-3" },
                  [
                    _c(
                      "v-btn",
                      {
                        attrs: { color: "success" },
                        on: {
                          click: function($event) {
                            $event.stopPropagation()
                            return _vm.save()
                          }
                        }
                      },
                      [_vm._v("Guardar")]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        ],
        1
      )
    : _c("div", [_c("Progress")], 1)
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/dashboard/pages/categories/form.vue":
/*!****************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/categories/form.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _form_vue_vue_type_template_id_59c817bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./form.vue?vue&type=template&id=59c817bc& */ "./resources/js/views/dashboard/pages/categories/form.vue?vue&type=template&id=59c817bc&");
/* harmony import */ var _form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/pages/categories/form.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _form_vue_vue_type_template_id_59c817bc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _form_vue_vue_type_template_id_59c817bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/pages/categories/form.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/pages/categories/form.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/categories/form.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./form.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/categories/form.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/pages/categories/form.vue?vue&type=template&id=59c817bc&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/categories/form.vue?vue&type=template&id=59c817bc& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_59c817bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./form.vue?vue&type=template&id=59c817bc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/categories/form.vue?vue&type=template&id=59c817bc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_59c817bc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_59c817bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);