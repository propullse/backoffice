(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[21],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/session/SignUpOne.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/session/SignUpOne.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Constants_AppConfig__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Constants/AppConfig */ "./resources/js/constants/AppConfig.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      valid: false,
      name: "",
      nameRules: [function (v) {
        return !!v || "Name is required";
      }, function (v) {
        return v.length <= 20 || "Name must be less than 20 characters";
      }],
      email: "",
      emailRules: [function (v) {
        return !!v || "E-mail is required";
      }, function (v) {
        return /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || "E-mail must be valid";
      }],
      password: "",
      passwordRules: [function (v) {
        return !!v || "Password is required";
      }],
      appLogo: Constants_AppConfig__WEBPACK_IMPORTED_MODULE_0__["default"].appLogo2,
      brand: Constants_AppConfig__WEBPACK_IMPORTED_MODULE_0__["default"].brand
    };
  },
  methods: {
    submit: function submit() {
      if (this.valid) {
        var userDetail = {
          name: this.name,
          email: this.email,
          password: this.password
        };
        this.$store.dispatch("signupUserInFirebase", {
          userDetail: userDetail,
          router: this.$router
        });
      }
    },
    signupWithLaravel: function signupWithLaravel() {
      var userDetail = {
        name: this.name,
        email: this.email,
        password: this.password
      };
      this.$store.dispatch("signupUserWithLaravelPassport", {
        userDetail: userDetail
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/session/SignUpOne.vue?vue&type=template&id=b0928f74&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/session/SignUpOne.vue?vue&type=template&id=b0928f74& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "session-wrapper" }, [
    _c(
      "div",
      {
        staticClass: "session-right text-center",
        staticStyle: { margin: "0px" }
      },
      [
        _c("div", { staticClass: "session-table-cell" }, [
          _c(
            "div",
            { staticClass: "session-content" },
            [
              _c("img", {
                staticClass: "img-responsive mb-4",
                attrs: { src: _vm.appLogo, width: "200", height: "200" }
              }),
              _vm._v(" "),
              _c("h2", { staticClass: "mb-4" }, [
                _vm._v(_vm._s(_vm.$t("message.signUp")))
              ]),
              _vm._v(" "),
              _c(
                "p",
                { staticClass: "fs-14" },
                [
                  _vm._v(
                    "\n          " +
                      _vm._s(_vm.$t("message.havingAnAccount")) +
                      "\n          "
                  ),
                  _c("router-link", { attrs: { to: "/dashboard/login" } }, [
                    _vm._v(_vm._s(_vm.$t("message.login")))
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-form",
                {
                  staticClass: "mb-5",
                  model: {
                    value: _vm.valid,
                    callback: function($$v) {
                      _vm.valid = $$v
                    },
                    expression: "valid"
                  }
                },
                [
                  _c("v-text-field", {
                    attrs: {
                      label: "Username",
                      rules: _vm.nameRules,
                      counter: 20,
                      required: ""
                    },
                    model: {
                      value: _vm.name,
                      callback: function($$v) {
                        _vm.name = $$v
                      },
                      expression: "name"
                    }
                  }),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      label: "E-mail ID",
                      rules: _vm.emailRules,
                      required: ""
                    },
                    model: {
                      value: _vm.email,
                      callback: function($$v) {
                        _vm.email = $$v
                      },
                      expression: "email"
                    }
                  }),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      label: "Password",
                      rules: _vm.passwordRules,
                      type: "password",
                      required: ""
                    },
                    model: {
                      value: _vm.password,
                      callback: function($$v) {
                        _vm.password = $$v
                      },
                      expression: "password"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      staticClass: "mb-4",
                      attrs: { large: "", block: "", color: "primary" },
                      on: { click: _vm.signupWithLaravel }
                    },
                    [_vm._v(_vm._s(_vm.$t("message.signUp")))]
                  )
                ],
                1
              )
            ],
            1
          )
        ])
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/session/SignUpOne.vue":
/*!**************************************************!*\
  !*** ./resources/js/views/session/SignUpOne.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SignUpOne_vue_vue_type_template_id_b0928f74___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SignUpOne.vue?vue&type=template&id=b0928f74& */ "./resources/js/views/session/SignUpOne.vue?vue&type=template&id=b0928f74&");
/* harmony import */ var _SignUpOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SignUpOne.vue?vue&type=script&lang=js& */ "./resources/js/views/session/SignUpOne.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SignUpOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SignUpOne_vue_vue_type_template_id_b0928f74___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SignUpOne_vue_vue_type_template_id_b0928f74___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/session/SignUpOne.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/session/SignUpOne.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/views/session/SignUpOne.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SignUpOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SignUpOne.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/session/SignUpOne.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SignUpOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/session/SignUpOne.vue?vue&type=template&id=b0928f74&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/session/SignUpOne.vue?vue&type=template&id=b0928f74& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SignUpOne_vue_vue_type_template_id_b0928f74___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SignUpOne.vue?vue&type=template&id=b0928f74& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/session/SignUpOne.vue?vue&type=template&id=b0928f74&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SignUpOne_vue_vue_type_template_id_b0928f74___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SignUpOne_vue_vue_type_template_id_b0928f74___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);