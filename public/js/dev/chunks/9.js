(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/catalogs/form.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/catalogs/form.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['id'],
  components: {},
  data: function data() {
    return {
      url: window.baseUrl,
      loader: true,
      tab: 1,
      action: null,
      route: null,
      rules: {
        required: [function (value) {
          return !!value || "Campo obrigatório.";
        }]
      },
      form: {
        title: {},
        description: {}
      },
      formData: new FormData(),
      preview: {},
      errors: []
    };
  },
  mounted: function mounted() {
    if (this.id) {
      this.action = 'update';
      this.route = "/dashboard/catalogs/update/" + this.id;
      this.get();
    } else {
      this.action = 'store';
      this.route = "store";
      this.loader = false;
    }
  },
  methods: {
    get: function get() {
      var _this = this;

      axios.get('/dashboard/catalogs/fetch/' + this.id).then(function (response) {
        _this.form = response.data;
        _this.form.title = JSON.parse(response.data.title);
        _this.form.description = JSON.parse(response.data.description);

        if (_this.form.image) {
          _this.preview.image = _this.url + '/catalogs/' + _this.form.image;
          _this.form.image = null;
        }

        if (_this.form.file) {
          _this.preview.file = _this.url + '/catalogs/' + _this.form.file;
          _this.form.file = null;
        }

        _this.form.order = response.data.order;
        _this.loader = false;
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    previewImages: function previewImages() {
      if (this.form.image) {
        this.preview.image = URL.createObjectURL(this.form.image);
      }
    },
    save: function save() {
      var _this2 = this;

      this.$refs.form.validate();
      Object.keys(this.form).map(function (key, value) {
        if (_this2.form[key] != null) {
          if (key == 'title' || key == 'subtitle' || key == 'description') {
            _this2.formData.append(key, JSON.stringify(_this2.form[key]));
          } else {
            _this2.formData.append(key, _this2.form[key]);
          }
        }
      });
      axios.post(this.route, this.formData).then(function (response) {
        _this2.loader = false;

        _this2.$refs.flash.handler(true, 'success', 'Guardado com sucesso!');

        setTimeout(function () {
          return window.location.href = "/dashboard/catalogs";
        }, 1500);
      })["catch"](function (error) {
        console.log(error);

        if (error.response.status == 422) {
          _this2.$refs.flash.handler(true, 'error', 'Campos em falta!');

          _this2.errors = [];
          Object.keys(error.response.data.errors).map(function (key) {
            _this2.errors.push(key);
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/catalogs/form.vue?vue&type=template&id=13c84562&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/catalogs/form.vue?vue&type=template&id=13c84562& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.loader == false
    ? _c(
        "div",
        [
          _c("Flash", { ref: "flash" }),
          _vm._v(" "),
          _c(
            "app-card",
            { staticClass: "col-sm-12" },
            [
              _c("div", [_c("h3", [_vm._v("Adicionar")])]),
              _vm._v(" "),
              _c(
                "v-tabs",
                { staticClass: "col-sm-12" },
                [
                  _c(
                    "v-tab",
                    {
                      on: {
                        click: function($event) {
                          _vm.tab = 1
                        }
                      }
                    },
                    [_vm._v("Título")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab",
                    {
                      on: {
                        click: function($event) {
                          _vm.tab = 2
                        }
                      }
                    },
                    [_vm._v("Media")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab",
                    {
                      on: {
                        click: function($event) {
                          _vm.tab = 3
                        }
                      }
                    },
                    [_vm._v("Ordem")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("v-form", { ref: "form", staticClass: "pa-5" }, [
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.tab == 1,
                        expression: "tab == 1"
                      }
                    ]
                  },
                  _vm._l(this.$store.getters.languages, function(item) {
                    return _c(
                      "div",
                      { key: "item" + item.locale, staticClass: "mb-3 pb-3" },
                      [
                        _c("v-text-field", {
                          staticClass: "pb-3",
                          attrs: { label: "Título " + item.name },
                          model: {
                            value: _vm.form.title[item.locale],
                            callback: function($$v) {
                              _vm.$set(_vm.form.title, item.locale, $$v)
                            },
                            expression: "form.title[item.locale]"
                          }
                        }),
                        _vm._v(" "),
                        _c("v-text-field", {
                          staticClass: "pb-3",
                          attrs: { label: "Descrição " + item.name },
                          model: {
                            value: _vm.form.description[item.locale],
                            callback: function($$v) {
                              _vm.$set(_vm.form.description, item.locale, $$v)
                            },
                            expression: "form.description[item.locale]"
                          }
                        })
                      ],
                      1
                    )
                  }),
                  0
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.tab == 2,
                        expression: "tab == 2"
                      }
                    ]
                  },
                  [
                    _c("v-file-input", {
                      staticClass: "mb-3",
                      attrs: {
                        label: "Imagem",
                        "prepend-icon": "",
                        dense: "",
                        counter: "",
                        "show-size": "",
                        rules: _vm.rules.required
                      },
                      on: { change: _vm.previewImages },
                      model: {
                        value: _vm.form.image,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "image", $$v)
                        },
                        expression: "form.image"
                      }
                    }),
                    _vm._v(" "),
                    _vm.preview.image
                      ? _c("v-img", {
                          staticClass: "mb-6",
                          attrs: { src: _vm.preview.image, width: "200px" }
                        })
                      : _vm._e(),
                    _vm._v(" "),
                    _c("v-file-input", {
                      staticClass: "mb-0 pb-0 pt-6",
                      attrs: {
                        label: "Ficheiro",
                        "prepend-icon": "",
                        dense: "",
                        counter: "",
                        "show-size": "",
                        rules: _vm.rules.required
                      },
                      on: { change: _vm.previewImages },
                      model: {
                        value: _vm.form.file,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "file", $$v)
                        },
                        expression: "form.file"
                      }
                    }),
                    _vm._v(" "),
                    !_vm.form.file
                      ? _c(
                          "span",
                          {
                            staticClass: "col-sm-12 pb-6 pl-0",
                            staticStyle: { "font-size": "11px" }
                          },
                          [_vm._v(_vm._s(_vm.preview.file))]
                        )
                      : _vm._e()
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.tab == 3,
                        expression: "tab == 3"
                      }
                    ]
                  },
                  [
                    _c("v-text-field", {
                      staticClass: "pb-3",
                      attrs: { label: "Ordem" },
                      model: {
                        value: _vm.form.order,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "order", $$v)
                        },
                        expression: "form.order"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-sm-12 pl-0 ml-0 pt-3" },
                  [
                    _c(
                      "v-btn",
                      {
                        attrs: { color: "success" },
                        on: {
                          click: function($event) {
                            $event.stopPropagation()
                            return _vm.save()
                          }
                        }
                      },
                      [_vm._v("Guardar")]
                    )
                  ],
                  1
                )
              ])
            ],
            1
          )
        ],
        1
      )
    : _c("div", [_c("Progress")], 1)
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/dashboard/catalogs/form.vue":
/*!********************************************************!*\
  !*** ./resources/js/views/dashboard/catalogs/form.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _form_vue_vue_type_template_id_13c84562___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./form.vue?vue&type=template&id=13c84562& */ "./resources/js/views/dashboard/catalogs/form.vue?vue&type=template&id=13c84562&");
/* harmony import */ var _form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/catalogs/form.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _form_vue_vue_type_template_id_13c84562___WEBPACK_IMPORTED_MODULE_0__["render"],
  _form_vue_vue_type_template_id_13c84562___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/catalogs/form.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/catalogs/form.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/dashboard/catalogs/form.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./form.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/catalogs/form.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/catalogs/form.vue?vue&type=template&id=13c84562&":
/*!***************************************************************************************!*\
  !*** ./resources/js/views/dashboard/catalogs/form.vue?vue&type=template&id=13c84562& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_13c84562___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./form.vue?vue&type=template&id=13c84562& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/catalogs/form.vue?vue&type=template&id=13c84562&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_13c84562___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_form_vue_vue_type_template_id_13c84562___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);