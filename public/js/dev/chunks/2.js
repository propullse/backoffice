(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/categories/index.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/categories/index.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['page'],
  components: {},
  data: function data() {
    return {
      loader: true,
      categories: {},
      headers: [{
        text: "Página"
      }, {
        text: "Opções"
      }]
    };
  },
  mounted: function mounted() {
    this.get();
  },
  methods: {
    get: function get() {
      var _this = this;

      this.loader = true;
      axios.get('/dashboard/pages/categories/get').then(function (response) {
        _this.categories = response.data;
        _this.loader = false;
      })["catch"](function (errors) {
        console.log(errors);
      });
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/custom-page.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/custom-page.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['page'],
  components: {},
  data: function data() {
    return {
      loader: true,
      tab: 'en',
      options: {
        placeholder: 'Conteúdo da página...'
      },
      data: {},
      errors: []
    };
  },
  mounted: function mounted() {
    this.get();
  },
  methods: {
    get: function get() {
      var _this = this;

      this.loader = true;
      axios.get('/dashboard/pages/fetch/' + this.page).then(function (response) {
        if (Object.keys(response.data).length > 0) {
          _this.data = response.data;
          _this.data.title = JSON.parse(response.data.title);
          _this.data.content = JSON.parse(response.data.content);
        } else {
          _this.data = {
            url: _this.page,
            title: {},
            content: {}
          };
        }

        _this.loader = false;
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    save: function save() {
      var _this2 = this;

      this.loader = true;
      axios.post("/dashboard/pages/store/" + this.page, this.data).then(function (response) {
        _this2.$refs.flash.handler(true, 'success', 'Guardado com sucesso!');

        setTimeout(function () {
          return location.reload();
        }, 500);
      })["catch"](function (error) {
        console.log(error);

        if (error.response.status == 422) {
          _this2.errors = [];
          Object.keys(error.response.data.errors).map(function (key) {
            _this2.errors.push(key);
          });
        }
      });
    }
  },
  watch: {
    page: {
      deep: true,
      handler: function handler() {
        this.get();
        this.$refs.tab[0].$el.click();
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/index.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/index.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _homepage_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./homepage.vue */ "./resources/js/views/dashboard/pages/homepage.vue");
/* harmony import */ var _custom_page_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./custom-page.vue */ "./resources/js/views/dashboard/pages/custom-page.vue");
/* harmony import */ var _categories_index_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./categories/index.vue */ "./resources/js/views/dashboard/pages/categories/index.vue");
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['page'],
  components: {
    homepage: _homepage_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    customPage: _custom_page_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    categories: _categories_index_vue__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      url: window.baseUrl,
      loader: true
    };
  },
  mounted: function mounted() {
    this.loader = false;
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/categories/index.vue?vue&type=template&id=11fa7738&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/categories/index.vue?vue&type=template&id=11fa7738& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Flash", { ref: "flash" }),
      _vm._v(" "),
      _vm.loader == false
        ? _c(
            "div",
            [
              _c("app-card", { staticClass: "col-md-12" }, [
                _c("h3", { staticStyle: { "text-transform": "capitalize" } }, [
                  _vm._v("Categorias")
                ])
              ]),
              _vm._v(" "),
              _c(
                "app-card",
                { staticClass: "col-md-12" },
                [
                  _c("v-data-table", {
                    attrs: {
                      headers: _vm.headers,
                      items: _vm.categories,
                      "hide-default-footer": true
                    },
                    scopedSlots: _vm._u(
                      [
                        {
                          key: "item",
                          fn: function(ref) {
                            var item = ref.item
                            return [
                              _c("tr", [
                                _c("td", [_vm._v(_vm._s(item.category.name))]),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        attrs: {
                                          to: {
                                            name: "categoriesForm",
                                            params: {
                                              page: item.category.link_rewrite,
                                              page_id: item.id_category
                                            }
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "button",
                                          {
                                            staticClass:
                                              "mx-0 v-btn v-btn--flat v-btn--icon v-btn--round theme--light v-size--default",
                                            attrs: { type: "button" }
                                          },
                                          [
                                            _c(
                                              "span",
                                              { staticClass: "v-btn__content" },
                                              [
                                                _c(
                                                  "i",
                                                  {
                                                    staticClass:
                                                      "v-icon notranslate primary--text material-icons theme--light",
                                                    attrs: {
                                                      "aria-hidden": "true"
                                                    }
                                                  },
                                                  [_vm._v("edit")]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              ])
                            ]
                          }
                        }
                      ],
                      null,
                      false,
                      252926929
                    )
                  })
                ],
                1
              )
            ],
            1
          )
        : _c("div", [_c("Progress")], 1)
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/custom-page.vue?vue&type=template&id=59768a36&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/custom-page.vue?vue&type=template&id=59768a36& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Flash", { ref: "flash" }),
      _vm._v(" "),
      _vm.loader == false
        ? _c(
            "div",
            [
              _c("app-card", { staticClass: "col-md-12" }, [
                _c("h3", { staticStyle: { "text-transform": "capitalize" } }, [
                  _vm._v(_vm._s(this.page.replace("-", " ")))
                ])
              ]),
              _vm._v(" "),
              _c(
                "app-card",
                { staticClass: "col-md-12" },
                [
                  _c(
                    "v-tabs",
                    { staticClass: "mb-3 pb-3" },
                    _vm._l(this.$store.getters.languages, function(item) {
                      return _c(
                        "v-tab",
                        {
                          key: "lang_" + item.locale,
                          ref: "tab",
                          refInFor: true,
                          attrs: { index: "lang_" + item.locale },
                          on: {
                            click: function($event) {
                              _vm.tab = item.locale
                            }
                          }
                        },
                        [_vm._v(_vm._s(item.name))]
                      )
                    }),
                    1
                  ),
                  _vm._v(" "),
                  _vm._l(this.$store.getters.languages, function(item) {
                    return _c("v-text-field", {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.tab == item.locale,
                          expression: "tab == item.locale"
                        }
                      ],
                      key: "title_" + item.locale,
                      attrs: { label: "Título" },
                      model: {
                        value: _vm.data.title[item.locale],
                        callback: function($$v) {
                          _vm.$set(_vm.data.title, item.locale, $$v)
                        },
                        expression: "data.title[item.locale]"
                      }
                    })
                  }),
                  _vm._v(" "),
                  _vm._l(this.$store.getters.languages, function(item) {
                    return _c("quill-editor", {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.tab == item.locale,
                          expression: "tab == item.locale"
                        }
                      ],
                      key: "content_" + item.locale,
                      ref: "editor",
                      refInFor: true,
                      attrs: { options: _vm.options },
                      model: {
                        value: _vm.data.content[item.locale],
                        callback: function($$v) {
                          _vm.$set(_vm.data.content, item.locale, $$v)
                        },
                        expression: "data.content[item.locale]"
                      }
                    })
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "app-card",
                { staticClass: "col-md-12" },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "success" },
                      on: {
                        click: function($event) {
                          $event.stopPropagation()
                          return _vm.save()
                        }
                      }
                    },
                    [_vm._v("Guardar")]
                  )
                ],
                1
              )
            ],
            1
          )
        : _c("div", [_c("Progress")], 1)
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/index.vue?vue&type=template&id=44e19b46&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/pages/index.vue?vue&type=template&id=44e19b46& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.loader == false
    ? _c(
        "div",
        [
          _vm.page == "homepage"
            ? _c("homepage")
            : _vm.page == "categories"
            ? _c("categories")
            : _c("customPage", { attrs: { page: _vm.page } })
        ],
        1
      )
    : _c("div", [_c("Progress")], 1)
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/dashboard/pages/categories/index.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/categories/index.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_11fa7738___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=11fa7738& */ "./resources/js/views/dashboard/pages/categories/index.vue?vue&type=template&id=11fa7738&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/pages/categories/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_11fa7738___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_11fa7738___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/pages/categories/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/pages/categories/index.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/categories/index.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/categories/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/pages/categories/index.vue?vue&type=template&id=11fa7738&":
/*!************************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/categories/index.vue?vue&type=template&id=11fa7738& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_11fa7738___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=11fa7738& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/categories/index.vue?vue&type=template&id=11fa7738&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_11fa7738___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_11fa7738___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/dashboard/pages/custom-page.vue":
/*!************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/custom-page.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _custom_page_vue_vue_type_template_id_59768a36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./custom-page.vue?vue&type=template&id=59768a36& */ "./resources/js/views/dashboard/pages/custom-page.vue?vue&type=template&id=59768a36&");
/* harmony import */ var _custom_page_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./custom-page.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/pages/custom-page.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _custom_page_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _custom_page_vue_vue_type_template_id_59768a36___WEBPACK_IMPORTED_MODULE_0__["render"],
  _custom_page_vue_vue_type_template_id_59768a36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/pages/custom-page.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/pages/custom-page.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/custom-page.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_custom_page_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./custom-page.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/custom-page.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_custom_page_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/pages/custom-page.vue?vue&type=template&id=59768a36&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/custom-page.vue?vue&type=template&id=59768a36& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_custom_page_vue_vue_type_template_id_59768a36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./custom-page.vue?vue&type=template&id=59768a36& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/custom-page.vue?vue&type=template&id=59768a36&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_custom_page_vue_vue_type_template_id_59768a36___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_custom_page_vue_vue_type_template_id_59768a36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/dashboard/pages/index.vue":
/*!******************************************************!*\
  !*** ./resources/js/views/dashboard/pages/index.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_44e19b46___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=44e19b46& */ "./resources/js/views/dashboard/pages/index.vue?vue&type=template&id=44e19b46&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/pages/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_44e19b46___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_44e19b46___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/pages/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/pages/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/pages/index.vue?vue&type=template&id=44e19b46&":
/*!*************************************************************************************!*\
  !*** ./resources/js/views/dashboard/pages/index.vue?vue&type=template&id=44e19b46& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_44e19b46___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=44e19b46& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/pages/index.vue?vue&type=template&id=44e19b46&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_44e19b46___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_44e19b46___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);