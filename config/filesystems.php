<?php

if ( request()->getHttpHost() == 'backoffice.flametluceluminaires.com' ) {
    $public = base_path('../public_html');
}
else {
    $public = 'C:\laragon\www\flame-site\public';
}


return [

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => $public,
            'url' => $public,
            'visibility' => 'public',
        ],

        'homepage' => [
            'driver' => 'local',
            'root' => $public . '/images/homepage',
            'url' => $public . '/images/homepage'
        ],

        'manufacturers' => [
            'driver' => 'local',
            'root' => $public . '/images/manufacturers',
            'url' => $public . '/images/manufacturers',
        ],

        'newsletters' => [
            'driver' => 'local',
            'root' => $public . '/newsletters',
            'url' => $public . '/newsletters',
        ],

        'catalogs' => [
            'driver' => 'local',
            'root' => $public . '/catalogs',
            'url' => $public . '/catalogs',
        ],

        'categories' => [
            'driver' => 'local',
            'root' => $public . '/categories',
            'url' => $public . '/categories',
        ]
    ],

];
