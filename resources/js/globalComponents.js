//Vuely Global Components

import VuePerfectScrollbar from "vue-perfect-scrollbar";
import AppSectionLoader from "Components/AppSectionLoader/AppSectionLoader";
import { RotateSquare2 } from "vue-loading-spinner";
import PageTitleBar from "Components/PageTitleBar/PageTitleBar";
import AppCard from 'Components/AppCard/AppCard';
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';

import Flash from "Views/dashboard/components/FlashMessage.vue";
import Delete from "Components/DeleteConfirmationDialog/DeleteConfirmationDialog";
import Progress from "Views/dashboard/components/Progress.vue";
import Dropzone from "vue2-dropzone";


const GlobalComponents = {
   install(Vue) {
      Vue.component('appCard', AppCard);
      Vue.component('vuePerfectScrollbar', VuePerfectScrollbar);
      Vue.component('appSectionLoader', AppSectionLoader);
      Vue.component('pageTitleBar', PageTitleBar);
      Vue.component('rotateSquare2', RotateSquare2);
      Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);
      Vue.component('Flash', Flash);
      Vue.component('Delete', Delete);
      Vue.component('Dropzone', Dropzone);
      Vue.component('Progress', Progress);
   }
}

export default GlobalComponents
