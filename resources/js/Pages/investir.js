export default function() {
    //==================================================================
    // Investir Chart
    //==================================================================
    if ($("#investirChartDiv").length) {
        var showChart = true;
        var startChart = function(event) {
            function am4themes_myTheme(target) {
                if (target instanceof am4core.ColorSet) {
                    target.list = [am4core.color("#203A71")];
                }
            }
            // Themes begin
            am4core.useTheme(am4themes_myTheme);
            am4core.useTheme(am4themes_animated);
            // Themes end

            var chart = am4core.create("investirChartDiv", am4charts.XYChart);

            chart.data = [
                {
                    year: "Qualidade de Vida",
                    income: 90
                },
                {
                    year: "Estabilidade do clima social",
                    income: 79
                },
                {
                    year: "Infraestrutura de telecomunicações",
                    income: 73
                },
                {
                    year: "Nível de competências laborais",
                    income: 72
                },
                {
                    year: "Custos laborais",
                    income: 71
                }
            ];

            //create category axis for years
            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "year";
            categoryAxis.renderer.inversed = true;
            categoryAxis.renderer.grid.template.strokeOpacity = 0;

            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.labels.template.fontSize = 10;
            categoryAxis.renderer.minGridDistance = 10;

            //create value axis for income and expenses
            var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
            valueAxis.max = 100;
            valueAxis.renderer.minGridDistance = 100;
            valueAxis.numberFormatter.numberFormat = "#'%'";
            valueAxis.renderer.opposite = false;

            //create columns
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.categoryY = "year";
            series.dataFields.valueX = "income";
            series.name = "Income";
            series.columns.template.strokeOpacity = 0;

            //add chart cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.behavior = "zoomY";
            chart.responsive.enabled = true;

            $("[aria-labelledby]")[0].style.display = "none";
        };

        $(document).on("scroll", function() {
            var chartdiv = JSON.parse(
                JSON.stringify(
                    $("#investirChartDiv")[0].getBoundingClientRect()
                )
            );
            chartdiv.top += window.scrollY;
            if (
                $(this).scrollTop() + (screen.height / 3) * 2 >= chartdiv.top &&
                showChart
            ) {
                startChart();
                showChart = false;
            }
        });
    }

    //==================================================================
    // Side Tabs - Content
    //==================================================================
    $("ul.side-tabs li a").click(function() {
        var tab_id = $(this).attr("data-tab");

        $("ul.side-tabs li a").removeClass("active");
        $(".side-tabs-content").removeClass("active");

        $(this).addClass("active");
        $("#" + tab_id).addClass("active");
    });
}
