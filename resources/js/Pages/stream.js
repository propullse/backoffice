export default function() {
    //==================================================================
    // Stream Chart
    //==================================================================
    if ($("#streamChartDiv").length) {
        var showChart = true;
        var startChart = function(event) {
            function am4themes_myTheme(target) {
                if (target instanceof am4core.ColorSet) {
                    target.list = [am4core.color("#203A71")];
                }
            }
            // Themes begin
            am4core.useTheme(am4themes_myTheme);
            am4core.useTheme(am4themes_animated);
            // Themes end

            var chart = am4core.create("streamChartDiv", am4charts.XYChart);

            chart.data = [
                {
                    year: "Cerâmica & Vidro",
                    income: 3
                },
                {
                    year: "Pedra Natural",
                    income: 14
                },
                {
                    year: "TIC",
                    income: 5
                },
                {
                    year: "Energia",
                    income: 2
                },
                {
                    year: "Contrução",
                    income: 2
                },
                {
                    year: "Aeronáutica",
                    income: 2
                },
                {
                    year: "Comércio & Serviços",
                    income: 5
                },
                {
                    year: "Moldes & Plásticos",
                    income: 30
                },
                {
                    year: "Turismo",
                    income: 4
                },
                {
                    year: "Têxtil & Calçado",
                    income: 2
                },
                {
                    year: "Metalomecânica",
                    income: 20
                },
                {
                    year: "Madeira & Mobiliário",
                    income: 6
                },
                {
                    year: "Indústria Alimentar",
                    income: 5
                }
            ];

            //create category axis for years
            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "year";
            categoryAxis.renderer.inversed = true;
            categoryAxis.renderer.grid.template.strokeOpacity = 0;

            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.labels.template.fontSize = 10;
            categoryAxis.renderer.minGridDistance = 10;

            //create value axis for income and expenses
            var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
            valueAxis.max = 50;
            valueAxis.renderer.minGridDistance = 50;
            valueAxis.numberFormatter.numberFormat = "#'%'";
            valueAxis.renderer.opposite = false;

            //create columns
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.categoryY = "year";
            series.dataFields.valueX = "income";
            series.name = "Income";
            series.columns.template.strokeOpacity = 0;

            //add chart cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.behavior = "zoomY";
            chart.responsive.enabled = true;

            $("[aria-labelledby]")[0].style.display = "none";
        };

        $(document).on("scroll", function() {
            var chartdiv = JSON.parse(
                JSON.stringify($("#streamChartDiv")[0].getBoundingClientRect())
            );
            chartdiv.top += window.scrollY;
            if (
                $(this).scrollTop() + (screen.height / 3) * 2 >= chartdiv.top &&
                showChart
            ) {
                startChart();
                showChart = false;
            }
        });
    }

    //==================================================================
    // Stream Header Vídeo
    //==================================================================

    if ($("#original_video").length) {
        var fsmActual = document.createElement("div");
        fsmActual.setAttribute("id", "fsm_actual");
        document.body.appendChild(fsmActual);
        var $fsm = document.querySelectorAll(".fsm");
        var $fsmActual = document.querySelector("#fsm_actual");
        $fsmActual.style.position = "absolute";
        var fullscreen = false;

        var position = {};
        var size = {};

        //modal action stuffs
        var openFSM = function(event) {
            var $this = event.currentTarget;
            position = $this.getBoundingClientRect();
            size = {
                width: window.getComputedStyle($this).width,
                height: window.getComputedStyle($this).height
            };

            $fsmActual.style.position = "absolute";
            $fsmActual.style.top = position.top + "px";
            $fsmActual.style.left = position.left + "px";
            $fsmActual.style.height = size.height;
            $fsmActual.style.width = size.width;
            $fsmActual.style.margin = $this.style.margin;

            setTimeout(function() {
                $fsmActual.innerHTML = $this.innerHTML;
                var classes = $this.classList.value.split(" ");
                for (var i = 0; i < classes.length; i++) {
                    $fsmActual.classList.add(classes[i]);
                }

                $("#fsm_actual").find(".video-container")[0].style.height =
                    "inherit";
                $("#fsm_actual").find(".play-btn")[0].style.display = "none";
                $("body").css("pointer-events", "none");
                $fsmActual.classList.add("growing");
                $fsmActual.style.height = "100vh";
                $fsmActual.style.width = "100vw";
                $fsmActual.style.top = "0";
                $fsmActual.style.left = "0";
                $fsmActual.style.margin = "0";
            }, 1);

            setTimeout(function() {
                $("#original_video").find(
                    ".video-container"
                )[0].style.visibility = "hidden";
                $fsmActual.classList.remove("growing");
                $fsmActual.classList.add("full-screen");
                $(window).scroll(function() {
                    if (fullscreen) closeFSM();
                });
                fullscreen = true;
                $("body").css("pointer-events", "all");
            }, 1000);
        };

        var closeFSM = function(event) {
            fullscreen = false;
            var $this = $("#fsm_actual")[0];

            var originalPosition = JSON.parse(
                JSON.stringify($("#original_video")[0].getBoundingClientRect())
            );
            originalPosition.top += window.scrollY;

            $this.style.height = size.height;
            $this.style.width = size.width;
            $this.style.top = originalPosition.top + "px";
            $this.style.left = originalPosition.left + "px";
            $this.style.margin = "0";
            $("body").css("pointer-events", "none");
            $this.classList.remove("full-screen");
            $this.classList.add("shrinking");
            $("#fsm_actual").find(".play-btn")[0].style.display = null;

            setTimeout(function() {
                $("#original_video").find(
                    ".video-container"
                )[0].style.visibility = null;
                while ($this.firstChild) $this.removeChild($this.firstChild);
                var classList = $this.classList;
                while (classList.length > 0) {
                    classList.remove(classList.item(0));
                }
                $this.style = "";
                $("body").css("pointer-events", "all");
            }, 1000);
        };

        for (var i = 0; i < $fsm.length; i++) {
            $fsm[i].addEventListener("click", openFSM);
        }
        $fsmActual.addEventListener("click", closeFSM);
        $("#fsm_actual").scroll(function() {
            //.box is the class of the div
            if (fullscreen) closeFSM();
        });
    }
}
