require("owl.carousel");
// import ScrollMagic from "scrollmagic";
// import "scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap";
// import gsap from "gsap/all";

export default function() {
    //==================================================================
    // Header Carousel
    //==================================================================
    let owl = $(".owl-carousel").owlCarousel({
        items: 1,
        loop: false,
        autoplay: true,
        rewind: true,
        autoplayTimeout: 8000
    });

    $(".slider-btn").click(function() {
        owl.trigger("to.owl.carousel", $(this).data().slide);
    });

    $(".next-slide-button").click(function() {
        owl.trigger("next.owl.carousel");
    });

    owl.on("changed.owl.carousel", function(event) {
        $(".new").removeClass("active");
        $(
            $('*[data-slide= "' + event.item.index + '"]').closest(".new")[0]
        ).addClass("active");
    });
    $($('*[data-slide="0"]').closest(".new")[0]).addClass("active");
}
