import Vue from 'vue'
import Router from 'vue-router'

//routes
import defaultRoutes from './default';

// session components
const SignUpOne = () =>
    import ('Views/session/SignUpOne');
const LoginOne = () =>
    import ('Views/session/LoginOne');
const LockScreen = () =>
    import ('Views/session/LockScreen');
const ForgotPassword = () =>
    import ('Views/session/ForgotPassword');
const ResetPassword = () =>
    import ('Views/session/ResetPassword');

const Auth0CallBack = () =>
    import ('Components/Auth0Callback/Auth0Callback');

Vue.use(Router)

export default new Router({
    mode: 'history',
    props: true,
    routes: [
        defaultRoutes,
        {
            path: '/',
            component: LoginOne,
            meta: {
                title: 'message.login',
                breadcrumb: null,
                requiresAuth: true
            }
        },
        {
            path: '/callback',
            component: Auth0CallBack
        },
        {
            path: '/dashboard/sign-up',
            component: SignUpOne,
            meta: {
                title: 'message.signUp',
                breadcrumb: null
            }
        },
        {
            path: '/dashboard/login',
            component: LoginOne,
            meta: {
                title: 'message.login',
                breadcrumb: null
            }
        },
        {
            path: '/dashboard/lock-screen',
            component: LockScreen,
            meta: {
                title: 'Lock Screen',
                breadcrumb: null
            }
        },
        {
            path: '/dashboard/forgot-password',
            component: ForgotPassword,
            meta: {
                title: 'message.forgotPassword',
                breadcrumb: null
            }
        },
        {
            path: '/dashboard/reset-password',
            component: ResetPassword,
            meta: {
                title: 'message.resetPassword',
                breadcrumb: null
            }
        }
    ]
})