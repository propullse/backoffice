import Full from 'Container/Full'

// dashboard components
const Dashboard = () => import ('Views/dashboard/Dashboard');

const Pages = () => import ('Views/dashboard/pages/index');

const Homepage = () => import ('Views/dashboard/pages/homepage');

const CategoriesForm = () => import ('Views/dashboard/pages/categories/form');

const Designers = () => import ('Views/dashboard/designers/index');
const DesignersForm = () => import ('Views/dashboard/designers/form');

const Newsletters = () => import ('Views/dashboard/newsletters/index');
const NewslettersForm = () => import ('Views/dashboard/newsletters/form');

const Catalogs = () => import ('Views/dashboard/catalogs/index');
const CatalogsForm = () => import ('Views/dashboard/catalogs/form');

const Translate = () => import ('Views/dashboard/translate/index');
const TranslateForm = () => import ('Views/dashboard/translate/form');

const Ambientes = () => import ('Views/dashboard/ambientes/index');
const AmbientesForm = () => import ('Views/dashboard/ambientes/form');

const Update = () => import ('Views/dashboard/update/index');

export default {
    path: '/',
    component: Full,
    props: true,
    redirect: '/dashboard',
    children: [
    {
        path: '/dashboard',
        component: Dashboard,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Dashboard'
        }
    },
    //Pages
    {
        path: '/dashboard/pages/:page',
        component: Pages,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Páginas'
        }
    },
    //Categorias
    {
        path: '/dashboard/pages/categories/:page/:page_id',
        name: 'categoriesForm',
        component: CategoriesForm,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Editar'
        }
    },
    //Designers
    {
        path: '/dashboard/designers/',
        component: Designers,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Designers'
        }
    },
    {
        path: '/dashboard/designers/:id',
        component: DesignersForm,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Designers'
        }
    },
    //Newsletters
    {
        path: '/dashboard/newsletters/',
        component: Newsletters,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Newsletters'
        }
    },
    {
        path: '/dashboard/newsletters/form',
        component: NewslettersForm,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Newsletters'
        }
    },
    {
        path: '/dashboard/newsletters/form/:id',
        name: 'NewslettersForm',
        component: NewslettersForm,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Newsletters'
        }
    },
    //Catalogs
    {
        path: '/dashboard/catalogs/',
        component: Catalogs,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Catálogos'
        }
    },
    {
        path: '/dashboard/catalogs/form',
        component: CatalogsForm,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Catálogos'
        }
    },
    {
        path: '/dashboard/catalogs/form/:id',
        name: 'CatalogsForm',
        component: CatalogsForm,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Catálogos'
        }
    },
    //Ambientes
    {
        path: '/dashboard/ambientes/',
        component: Ambientes,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Ambientes'
        }
    },
    {
        path: '/dashboard/ambientes/form',
        component: AmbientesForm,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Ambientes'
        }
    },
    {
        path: '/dashboard/ambientes/form/:id',
        name: 'AmbientesForm',
        component: AmbientesForm,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Ambientes'
        }
    },
    //Translate
    {
        path: '/dashboard/translate/',
        component: Translate,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Traduções'
        }
    },
    {
        path: '/dashboard/translate/form',
        component: TranslateForm,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Traduções'
        }
    },
    {
        path: '/dashboard/translate/form/:id',
        component: TranslateForm,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Traduções'
        }
    },
    //Update
    {
        path: '/dashboard/update/',
        component: Update,
        props: true,
        meta: {
            requiresAuth: true,
            title: 'Atualizar'
        }
    }
    ]
}