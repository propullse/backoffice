import axios from 'axios';

axios.defaults.headers.common['Access-Control-Allow-Headers'] = 'X-CSRF-Token';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

if (localStorage.getItem('access_token') !== null) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
}

let webService = axios.create({
    baseURL: '/api/auth'
});

export default webService;
