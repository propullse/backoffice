/**
 * App Config File
 */
export default {
    appLogo: '/images/logos/round_logo_white.svg', // App Logo,
    darkLogo: '/images/logos/logo.svg', // dark logo
    appLogo2: '/images/logos/logo.svg', // App Logo 2 For Login & Signup Page
    appLogoWhite: '/images/logos/round_logo_white.svg',
    brand: 'flam&luce', // Brand Name
    copyrightText: 'flam&luce © 2021 All Rights Reserved.', // Copyright Text
    enableUserTour: process.env.NODE_ENV === 'production' ? true : false, // Enable User Tour
    weatherApiId: 'b1b15e88fa797225412429c1c50c122a1', // weather API Id
    weatherApiKey: '69b72ed255ce5efad910bd946685883a' // weather APi key
}
