// Sidebar Routers
export const menus = {
    '': [
        //DASHBOARD
        {
            action: 'zmdi-apps',
            title: 'Dashboard',
            path: '/'
        },
        //PAGES
        {
            action: 'zmdi-file-text',
            title: 'Páginas',
            path: '/',
            items: [
            {
                title: 'Homepage',
                path: '/pages/homepage'
            },
            {
                title: 'Categorias',
                path: '/pages/categories'
            },
            {
                title: 'P2020',
                path: '/pages/p2020'
            }
            ,
            {
                title: 'Informações Legais',
                path: '/pages/informacoes-legais'
            },
            {
                title: 'Privacidade',
                path: '/pages/privacidade'
            }
            ]
        },
        //DESIGNERS
        {
            action: 'zmdi-account-circle',
            title: 'Designers',
            path: '/designers'
        },
        //NEWSLETTERS
        {
            action: 'zmdi-file-plus',
            title: 'Newsletters',
            path: '/newsletters'
        },
        //CATALOGOS
        {
            action: 'zmdi-download',
            title: 'Catálogos',
            path: '/catalogs'
        },
        //AMBIENTES
        {
            action: 'zmdi-collection-image',
            title: 'Ambientes',
            path: '/ambientes'
        },
        //TRADUÇÕES   
        {
            action: 'zmdi-text-format',
            title: 'Traduções',
            path: '/translate'
        },
        //UPDATE   
        {
            action: 'zmdi-refresh-alt',
            title: 'Atualizar Imagens',
            path: '/update'
        }
        ]
    }