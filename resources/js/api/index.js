import axios from 'axios';

axios.defaults.headers.common['Access-Control-Allow-Headers'] = 'X-CSRF-Token';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

if (localStorage.getItem('access_token') !== null) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
}

/*export default
    axios.create({
        baseURL: 'https://reactify.theironnetwork.org/data/'
    });*/

export default axios;
