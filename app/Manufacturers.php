<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Manufacturers_info;

class manufacturers extends Model {
	protected $table = 'ps_manufacturer';
	protected $primaryKey = 'id_manufacturer';
	protected $with = ['info'];

	public function info() {
		return $this->hasOne(Manufacturers_info::class, 'id_manufacturer', 'id_manufacturer');
	}

}
