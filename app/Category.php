<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'ps_category';
    protected $with = ['category_lang'];

    ### Relations ###

    public function category_lang() {
        return $this->hasMany(CategoryLang::class, 'id_category', 'id_category')->where('id_shop', 1)->where('id_lang', 3);
    }

    ### Scopes ###

    public function scopeActive() {
        return $this->where('active', 1);
    }

    public function scopeMenu() {
    	return $this->whereIn('id_category', [12, 17,47,48,50,53]);
    }
/*
    public function produtos() {
    	return $this->hasOne(CategoryProducts::class, 'id_category', 'id_category')->orderBy('position', 'ASC');
    }*/
}
