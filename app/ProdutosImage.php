<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutosImage extends Model {
    protected $table = 'ps_image';
    protected $with = ['lang'];

     public function lang() {
    	return $this->hasOne(ProdutosImageLang::class, 'id_image', 'id_image')->where('id_lang', Helper::id_lang()); 	
    }
}
