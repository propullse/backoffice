<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Produtos extends Model {
	
    protected $table = 'ps_product';
    protected $with = ['lang', 'attributes', 'manufacturer', 'categoriaMain', 'features', 'images', 'cover', 'ambiente'];

    public $timestamps = false;

    ### Relations ###

    public function attributes() {
        return $this->hasMany(ProdutosAttributes::class, 'id_product', 'id_product');
    }

    public function lang() {
    	return $this->hasOne(ProdutosLang::class, 'id_product', 'id_product')->where('id_shop', 1)->where('id_lang', Helper::id_lang());	
    }

    public function categorias() {
        return $this->hasMany(CategoryProducts::class, 'id_product', 'id_product');    
    }

    public function categoriaMain() {
        return $this->hasOne(CategoryLang::class, 'id_category', 'id_category_default')->where('id_shop', 1)->where('id_lang', Helper::id_lang()); 
    }

    public function features() {
        return $this->hasMany(ProdutosFeatures::class, 'id_product', 'id_product')->whereNotIn('id_feature', [16,17]);
    }

    public function images() {
    	return $this->hasMany(ProdutosImage::class, 'id_product', 'id_product')->whereHas('lang', function($query) {
            $query->whereNull('legend')->orWhere('legend', '!=', 'ambiente');
        });
    }

    public function cover() {
        return $this->hasOne(ProdutosImage::class, 'id_product', 'id_product')->where('cover', 1); 
    }

    public function ambiente() {
        return $this->hasOne(ProdutosImage::class, 'id_product', 'id_product')->where('cover', 1)->whereHas('lang', function($query) {
            $query->where('legend', 'ambiente');
        });
    }

    public function manufacturer() {
        return $this->hasOne(Manufacturer::class, 'id_manufacturer', 'id_manufacturer'); 
    }


    ### SCOPES ###

    public function scopeActive() {
    	return $this->where('id_shop_default', 1)->where('active', 1);
    }

/*
    public function scopeFindBySlug($slug) {
        dd($slug);
        return $this->join(DB::raw("(SELECT * FROM ps_product_lang WHERE id_shop = 1 AND id_lang = ".Helper::id_lang().") AS p_query"), 'p_query.id_product', '=', 'ps_product.id_product')->where('link_rewrite', $slug);
    }*/
}
