<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Helper;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        if ( request()->getHttpHost() == 'backoffice.flametluceluminaires.com' ) {
            $this->app->bind('path.public', function () {
                return base_path('../public_html');
            });
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {

    }
}
