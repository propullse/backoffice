<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutosFeatures extends Model
{
    protected $table = 'ps_feature_product';


    protected $with = ['item', 'value'];

    public function item() {
    	return $this->hasOne(ProdutosFeaturesLang::class, 'id_feature', 'id_feature')->where('id_lang', Helper::id_lang());	
    }

    public function value() {
    	return $this->hasOne(ProdutosFeaturesValueLang::class, 'id_feature_value', 'id_feature_value')->where('id_lang', Helper::id_lang());	
    }
}
