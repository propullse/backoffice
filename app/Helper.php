<?php

namespace App;

use Illuminate\Support\Facades\Route;

use Request;

use App\Langs;

class Helper {

	public static function id_lang($get = '') {

        if ( !empty($get) ) {
            $locale = $get;
        }
        else {
            $locale = app()->getLocale();
        }

        if ( $locale == 'en' ) {
            $id_lang = 1;
            $route_product = '/product/';
        }
        else if ( $locale == 'fr' ) {
            $id_lang = 2;
            $route_product = '/produit/';
        }
        else if ( $locale == 'pt' ) {
            $id_lang = 3;
            $route_product = '/produto/';
        }
        else if ( $locale == 'es' ) {
            $id_lang = 4;
            $route_product = '/producto/';
        }
        else if ( $locale == 'de' ) {
            $id_lang = 5;
            $route_product = '/produkt/';
        }
        else if ( $locale == 'it' ) {
            $id_lang = 6;
            $route_product = '/prodotto/';
        }
        else {
            $id_lang = 3;
            $route_product = '/produto/';
        }

        return $id_lang;
    }

}