<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutosImageLang extends Model {
    protected $table = 'ps_image_lang';
    protected $guarded = [];
    public $timestamps = false;
}
