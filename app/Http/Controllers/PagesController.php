<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Str;
use Storage;

use App\pages;

class PagesController extends Controller {

	public function fetch($url) {
		return pages::find($url);
	}

	public function store(Request $request, $url) {
		$page = pages::find($url);

		$data = $request->validate([
			'url' => '',
			'id_category' => '',
			'title' => '',
			'content' => ''
		]);


		if ( $request->is_category ) {

			if ( $request->action == 'store' ) {
				$request->validate([
					'imagem_esquerda' => 'required',
					'imagem_direita' => 'required',
				]);
			}

			

			if ( $page ) {
				$data['content']['imagem_esquerda'] = json_decode($page->content, true)['imagem_esquerda'];
				$data['content']['imagem_direita'] = json_decode($page->content, true)['imagem_direita'];
			}
			
			#Se tiver imagem esq.
			if ( $request->imagem_esquerda ) {
				if ( $page && json_decode($page->content, true)['imagem_esquerda'] ) {
					Storage::disk('categories')->delete(json_decode($page->content, true)['imagem_esquerda']);
				}

				$file = Str::random(40).'.'.$request->imagem_esquerda->getClientOriginalExtension();
				Storage::disk('categories')->put('/' . $file, file_get_contents($request->imagem_esquerda->getRealPath()));
				$data['content']['imagem_esquerda'] = $file;
			}


			#Se tiver imagem dir.
			if ( $request->imagem_direita ) {
				if ( $page && json_decode($page->content, true)['imagem_direita'] ) {
					Storage::disk('categories')->delete(json_decode($page->content, true)['imagem_direita']);
				}

				$file = Str::random(40).'.'.$request->imagem_direita->getClientOriginalExtension();
				Storage::disk('categories')->put('/' . $file, file_get_contents($request->imagem_direita->getRealPath()));
				$data['content']['imagem_direita'] = $file;
			}


			$data['content']['posicao'] =  $request->posicao;
		}


		if ( !empty($data['title']) ) {
			$data['title'] = json_encode($data['title'], true);
		}
		
		$data['content'] = json_encode($data['content'], true);


		if ( empty($page) ) {
			pages::create($data);
		}
		else {
			$page->update($data);
		}

	}
}
