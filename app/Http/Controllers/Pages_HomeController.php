<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use File;

use App\homepage;

class Pages_HomeController extends Controller {

    public function get() {
        return response()->json(homepage::first());
    }


    #SLIDER
    public function storeSlider(Request $request) {
        $slider = homepage::first();

        $request->validate([
            'file' => 'required',
            'ordem' => 'required'
        ]);

        $data = collect($slider->slider);

        $request['id'] = Str::random(12);
        $data[] = $request->all();

        $slider->update(['slider' => $data->sortBy('ordem')->values()]);

        return response("Sucesso", 201);
    }

    public function updateSlider(Request $request, $id) {
        $slider = homepage::first();

        $request->validate([
            'file' => 'required',
            'ordem' => 'required'
        ]);

        $put = collect($slider->slider)->map(function($item, $key) use ($request, $id) {
            return ( $item['id'] == $id ) ? $request->all() : $item;
        });

        $slider->update(['slider' => $put->sortBy('ordem')->values()]);

        return response("Sucesso", 201);
    }

    public function removeSlider(Request $request, $id) {
        $slider = homepage::first();

        $put = collect($slider->slider)->filter(function($item, $key) use ($request, $id) {
            return ( $item['id'] == $id ) ? Storage::disk('homepage')->delete($item['file']) : $item;
        });

        $slider->update(['slider' => $put->sortBy('ordem')->values()]);

        return response("Sucesso", 201);
    }

    public function uploadFileSlider(Request $request) {
        $file = Str::random(30).'.'.$request->file->getClientOriginalExtension();
        Storage::disk('homepage')->put($file, file_get_contents($request->file()['file']->getRealPath()));

        return $file;
    }


    public function removeFileSlider(Request $request) {
        Storage::disk($request->disk)->delete($request->file);
    }

    #TEXTO 1
    public function updateText(Request $request) {
        $request->validate([
            'pt' => '',
            'fr' => '',
            'en' => ''
        ]);

        $data = homepage::first();
        $data->text_one = $request->all();
        $data->save();

        return response("Sucesso", 201);
    }

    #sliderTwo
    public function sliderTwo(Request $request) {
        $slider_two = homepage::first();

        $request->validate([
            'title' => '',
            'manufacturer' => 'required',
            'ordem' => 'required',
            'posicao' => 'required',
            'files' => 'required'
        ]);

        $data = collect($slider_two->slider_two);

        $request['id'] = Str::random(12);
        $data[] = $request->all();

        $slider_two->update(['slider_two' => $data->sortBy('ordem')->values()]);

        return response("Sucesso", 201);
    }

    public function updateSliderTwo(Request $request, $id) {
        $slider = homepage::first();

        $request->validate([
            'file' => '',
            'ordem' => 'required'
        ]);

        $put = collect($slider->slider_two)->map(function($item, $key) use ($request, $id) {
            return ( $item['id'] == $id ) ? $request->all() : $item;
        });

        $slider->update(['slider_two' => $put->sortBy('ordem')->values()]);

        return response("Sucesso", 201);
    }

    public function removeSliderTwo(Request $request, $id) {
        $data = homepage::first();

        $put = collect($data->slider_two)->filter(function($item, $key) use ($request, $id) {
            if ( $item['id'] == $id ) {
                foreach ( $item['files'] as $image ) {
                    Storage::disk('homepage')->delete($image);
                }
            }
            else {
                return $item; 
            }
        });

        $data->update(['slider_two' => $put->sortBy('ordem')->values()]);

        return response("Sucesso", 201);
    }

    #VIDEO
    public function sliderVideo(Request $request) {
        $video = homepage::first();

        $insert = $request->validate([
            'title' => '',
            'subtitle' => '',
            'manufacturer' => '',
            'upload' => '',
            'image' => '',
            'file' => '',
            'ordem' => 'required',
            'url' => ''
        ]);


        $data = collect($video->video);

        $request['id'] = Str::random(12);

        if ( !empty($request->imagem)  && $request->imagem != "null" ) {
            $file = Str::random(40).'.'.$request->imagem->getClientOriginalExtension(); 
            Storage::disk('homepage')->put($file, file_get_contents($request->imagem->getRealPath()));
            $request['imagem'] = $file;
        }

        $data[] = $request->all();

        $video->update(['video' => $data->sortBy('ordem')->values()]);

        return response("Sucesso", 201);
    }


    public function updateSliderVideo(Request $request, $id) {
        $video = homepage::first();

        $insert = $request->validate([
            'title' => '',
            'subtitle' => '',
            'manufacturer' => '',
            'type' => '',
            'image' => '',
            'file' => '',
            'ordem' => 'required',
            'url' => ''
        ]);

        if ( !empty($request->imagem)  && $request->imagem != "null" ) {
            $file = Str::random(40).'.'.$request->imagem->getClientOriginalExtension(); 
            Storage::disk('homepage')->put($file, file_get_contents($request->imagem->getRealPath()));
            $request['imagem'] = $file;
        }

        $put = collect($video->video)->map(function($item, $key) use ($request, $id) {
            return ( $item['id'] == $id ) ? $request->all() : $item;
        });


        $video->update(['video' => $put->sortBy('ordem')->values()]);

        return response("Sucesso", 201);
    }

    public function removeSliderVideo(Request $request, $id) {
        $data = homepage::first();

        $put = collect($data->video)->filter(function($item, $key) use ($request, $id) {
            #dd($item['id'], $id);
            if ( $item['id'] == $id ) {
                Storage::disk('homepage')->delete($item['file']);
            }
            else {
                return $item; 
            }
        });

        $data->update(['video' => $put->sortBy('ordem')->values()]);

        return response("Sucesso", 201);
    }

    #DESIGNERS
    public function designers(Request $request) {
        $data = homepage::first();

        $request->validate([
            'manufacturer' => 'required',
            'ordem' => 'required'
        ]);

        $put = json_decode($data->slider_three, true);
        $put[$request->ordem] = json_encode($request->all());

        $data->slider_three = $put;
        $data->save();

        return response("Sucesso", 201);
    }

    public function removeSliderDesigners(Request $request, $ordem) {
        $data = homepage::first();

        $put = collect(json_decode($data->slider_three, true))->filter(function($item, $key) use ($ordem) {
            if ( $key != $ordem ) { 
                return $item; 
            }
        });

        $data->slider_three = $put;
        $data->save();

        return response("Sucesso", 201);
    }
}
