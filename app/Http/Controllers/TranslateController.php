<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Translate;

class TranslateController extends Controller {
    public function get() {
        return response()->json(Translate::orderBy('id', 'asc')->get());
    }

    public function fetch(Translate $translate) {
        return response()->json($translate);
    }

    public function store(Request $request) {

        $data = $request->validate([
            'id' => 'required',
            'value.*' => '',
            'value.pt' => 'required',
            'value.en' => 'required',
        ]);

        Translate::create($data);
    }

    public function update(Request $request, $translate) {

        $translate = Translate::find($translate);

        $data = $request->validate([
            'id' => 'required',
            'value.*' => '',
            'value.pt' => 'required',
            'value.en' => 'required',
        ]);

        if ( empty($translate) ) {
            Translate::create($data);
        }
        else {
            $translate->update($data);
        }
    }

    public function delete(Request $request, Translate $translate) {
        $translate->delete();
    }
}
