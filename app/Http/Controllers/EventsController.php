<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Events;
use App\Sub_events;
use App\List_events;

class EventsController extends Controller {

    public function fetchCategoria($id) {
        $event = Events::where('id', $id)->first();
        $event->name = json_decode($event->name);

        if ( count($event->sub_events) > 0 ) {
            foreach ( $event->sub_events as $data ) {
                $data->name = json_decode($data->name);
            }
        }

        return response()->json($event); 
    }

    public function fetchSubCategoria($id) {
        $data = Sub_events::where('id', $id)->first();
        $data->name = json_decode($data->name);

        return response()->json($data); 
    }

    public function fetch($id) {
        $event = List_events::where('id', $id)->first();
        $event->name = json_decode($event->name);

        return response()->json($event); 
    }

    public function getAll() {
        $events = Events::paginate(10);

        foreach ( $events as $item ) {
            $item->name = json_decode($item->name);

            if ( count($item->sub_events) > 0 ) {
                foreach ( $item->sub_events as $data ) {
                    $data->name = json_decode($data->name);

                    if ( count($data->list_events) > 0 ) {
                        foreach ( $data->list_events as $list_events ) {
                            $list_events->name = json_decode($list_events->name);
                        }
                    }
                }
            }


            if ( count($item->list_events) > 0 ) {
                foreach ( $item->list_events as $list_events ) {
                    $list_events->name = json_decode($list_events->name);
                }
            }
        } 

        return response()->json($events);	
    }

    public function getSubEvents() {
        $subEvents = Sub_events::all();

        return response()->json($subEvents);
    }


    public function createMainEvent(Request $request) {

        $data = $request->validate([
            'pt' => 'required',
            'en' => '',
        ]);

        $data['name'] = json_encode($data);

        Events::create($data);

        return response("success", 201);
    }


    public function createSubCategoriaEvent(Request $request) {

        $data = $request->validate([
            'id_events' => 'required',
            'name.pt' => 'required',
            'name.en' => '',
        ]);

        $data['name'] = json_encode($data['name']);

        Sub_events::create($data);

        return response("success", 201);
    }

    public function updateSubCategoria(Request $request, Sub_events $sub_events) {

        $data = $request->validate([
            'id_events' => 'required',
            'name.pt' => 'required',
            'name.en' => '',
        ]);

        $data['name'] = json_encode($data['name']);

        $sub_events->update($data);

        return response("success", 201);
    }


    public function updateCategoria(Request $request, Events $events) {

        $data = $request->validate([
            'id' => 'required',
            'name.pt' => 'required',
            'name.en' => '',
        ]);

        $data['name'] = json_encode($data['name']);

        $events->update($data);

        return response("success", 201);
    }


    public function store(Request $request) {

        $data = $request->validate([
            'id_events' => 'required',
            'id_sub_events' => '',
            'name.pt' => 'required',
            'name.en' => '',
            'date_start' => '',
            'date_end' => '',
        ]);

        $data['name'] = json_encode($data['name']);

        List_events::create($data);

        return response("success", 201);
    }

    public function delete($id) {
        $delete = List_events::find($id)->delete();
    }

    public function update(Request $request, List_events $event) {

        $data = $request->validate([
            'id_events' => '',
            'id_sub_events' => '',
            'name' => 'required',
            'date_start' => '',
            'date_end' => '',
        ]);
        
        $event->update($data);

        return response(['status' => 'success','data' => $event], 201);
    }

    public function categoriaDelete($id) {
        $categoria = Events::where('id', $id)->first();  

        foreach ( $categoria->sub_events as $delete_sub ) {
            $delete_sub->delete();
        }

        foreach ( $categoria->list_events as $delete_list ) {
            $delete_list->delete();
        }

        $categoria->delete();
    }

    public function subcategoriaDelete($id) {
        $sub_categoria = Sub_events::where('id', $id)->first();   

        foreach ( $sub_categoria->list_events as $delete_list ) {
            $delete_list->delete();
        }

        $sub_categoria->delete();
    }
}
