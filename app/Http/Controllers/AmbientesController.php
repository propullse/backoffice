<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Produtos;
use App\ProdutosImageLang;

class AmbientesController extends Controller {
    
    public function get() {
        return response()->json(Produtos::active()->whereHas('ambiente')->orderBy('id_product', 'asc')->get());
    }

    public function fetch(Request $request, $id) {
        return response()->json(Produtos::where('id_product', $id)->first());
    }

    public function update(Request $request, $id) {
        $data = $request->validate([
            'position_x' => '',
            'position_y' => ''
        ]);

        $image = ProdutosImageLang::where('id_image', $id)->update(['position_x' => $request->position_x, 'position_y' => $request->position_y]);

    }
}
