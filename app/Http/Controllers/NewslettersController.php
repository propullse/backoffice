<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

use App\newsletters;

class NewslettersController extends Controller {

	public function get() {
		return response()->json(newsletters::all());
	}

	public function fetch(newsletters $newsletter) {
		return response()->json($newsletter);
	}

	public function store(Request $request) {

		$data = $request->validate([
			'title' => '',
			'image' => 'required',
			'url' => '',
			'hex' => '',
			'order' => ''
		]);

		if ( is_null($data['order']) ) {
			$data['order'] = newsletters::orderBy('order', 'desc')->pluck('order')->first() + 1;
		}

		$image = Str::random(40).'.'.$request->image->getClientOriginalExtension();
		Storage::disk('newsletters')->put($image, file_get_contents($request->image->getRealPath()));
		
		$data['image'] = $image;

		newsletters::create($data);
	}


	public function update(Request $request, newsletters $newsletter) {
		$data = $request->validate([
			'title' => '',
			'image' => '',
			'url' => '',
			'hex' => '',
			'order' => ''
		]);

		if ( is_null($data['order']) ) {
			$data['order'] = newsletters::orderBy('order', 'desc')->pluck('order')->first() + 1;
		}

		if ( !empty($request->image) ) {
			$data['image'] = $newsletter->image;
			Storage::disk('newsletters')->put($newsletter->image, file_get_contents($request->image->getRealPath()));
		}
		
		$newsletter->update($data);
	}

	public function delete(Request $request, newsletters $newsletter) {
		Storage::disk('newsletters')->delete($newsletter->image);

		$newsletter->delete();
	}
}
