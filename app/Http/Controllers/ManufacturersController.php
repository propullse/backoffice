<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use File;

use App\Manufacturers;
use App\Manufacturers_info;

class ManufacturersController extends Controller
{
	public function get() {
		return response()->json(Manufacturers::join('manufacturers', 'manufacturers.id_manufacturer', '=', 'ps_manufacturer.id_manufacturer')->orderBy('manufacturers.order')->get());
	}

	public function fetch(Manufacturers $manufacturer) {
		return response()->json($manufacturer);
	}

	public function upload(Request $request, $id) {
		$file = Str::random(14).'.'.$request->file->getClientOriginalExtension();
		Storage::disk('manufacturers')->put($id.'/'.$file, file_get_contents($request->file->getRealPath()));

		return $file;
	}

	public function update(Request $request, $manufacturer) {
		$manufacturer = Manufacturers_info::find($manufacturer);

		$data = $request->validate([
			'id_manufacturer' => '',
			'texto' => '',
			'posicao' => '',
			'imagem_perfil' => '',
			'imagem_produto' => '',
			'imagem_assinatura' => '',
			'order' => ''
		]);

		empty($manufacturer) ? Manufacturers_info::create($data) : $manufacturer->update($data);
	}

	public function removeImage(Request $request, $type, $id) {
		$manufacturer = Manufacturers_info::find($id);

		if ( $type == 0 ) {
			$manufacturer->imagem_perfil = null;
		}
		else if ( $type == 1 ) {
			$manufacturer->imagem_produto = null;
		}
		else if ( $type == 2 ) {
			$manufacturer->imagem_assinatura = null;
		}

		$manufacturer->save();
	}
}
