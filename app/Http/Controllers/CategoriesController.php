<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\CategoryLang;

class CategoriesController extends Controller {

	public function get() {
		$data = Category::active()->menu()->get();

		foreach ( $data as $item ) {
			$item->category = $item->category_lang[0];
		}

		return response()->json($data);
	}

}
