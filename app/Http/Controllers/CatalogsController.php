<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

use App\catalogs;

class CatalogsController extends Controller
{
	public function get() {
		return response()->json(catalogs::orderBy('order', 'asc')->get());
	}

	public function fetch(catalogs $catalog) {
		return response()->json($catalog);
	}

	public function store(Request $request) {

		$data = $request->validate([
			'title' => '',
			'description' => '',
			'image' => 'required',
			'file' => 'required',
			'order' => 'required'
		]);

		$image = Str::random(40).'.'.$request->image->getClientOriginalExtension();
		Storage::disk('catalogs')->put($image, file_get_contents($request->image->getRealPath()));

		$file = Str::random(40).'.'.$request->file->getClientOriginalExtension();
		Storage::disk('catalogs')->put($file, file_get_contents($request->file->getRealPath()));
		
		$data['image'] = $image;
		$data['file'] = $file;

		catalogs::create($data);
	}

	public function update(Request $request, catalogs $catalog) {
		$data = $request->validate([
			'title' => '',
			'description' => '',
			'image' => '',
			'file' => '',
			'order' => 'required'
		]);

		if ( !empty($request->image) ) {
			$data['image'] = $catalog->image;
			Storage::disk('catalogs')->put($catalog->image, file_get_contents($request->image->getRealPath()));
		}
		
		if ( !empty($request->file) ) {
			Storage::disk('catalogs')->delete($catalog->file);

			$file = Str::random(40).'.'.$request->file->getClientOriginalExtension();
			Storage::disk('catalogs')->put($file, file_get_contents($request->file->getRealPath()));
			$data['file'] = $file;
		}
		
		$catalog->update($data);
	}

	public function delete(Request $request, catalogs $catalog) {

		Storage::disk('catalogs')->delete($catalog->image);
		Storage::disk('catalogs')->delete($catalog->file);

		$catalog->delete();
	}
}
