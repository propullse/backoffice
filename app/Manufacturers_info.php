<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class manufacturers_info extends Model
{
    protected $table = 'manufacturers';
    protected $primaryKey = 'id_manufacturer';
    public $timestamps = false;
    protected $guarded = [];

    protected $casts = [
        'texto' => 'array'
    ];
    
}
