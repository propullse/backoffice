<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class homepage extends Model {
	protected $table = 'homepage';
	public $timestamps = false;
	protected $guarded = [];

	protected $casts = [
		'slider' => 'array',
		'text_one' => 'array',
		'slider_two' => 'array',
		'video' => 'array'
	];
}
