<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Langs extends Model
{
    protected $table = 'ps_lang';
    protected $guard = [];
	public $timestamps = false;

	public function scopeActives() {
		return $this->where('active', 1);
	}
}
