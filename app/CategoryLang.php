<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryLang extends Model
{
    protected $table = 'ps_category_lang';
}
