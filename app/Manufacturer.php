<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model {
	protected $table = 'ps_manufacturer';
	protected $primaryKey = 'id_manufacturer';
	protected $with = ['data'];

	public function data() {
		return $this->hasOne(Manufacturers_info::class, 'id_manufacturer', 'id_manufacturer');
	}
}
