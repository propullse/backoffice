<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translate extends Model {
    protected $table = 'translate';

    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        'id' => 'string',
        'value' => 'array',
    ];
}
