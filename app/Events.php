<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
	protected $fillable = ['name'];
	protected $with = ['sub_events.list_events', 'list_events'];
	
    public function sub_events() {
        return $this->hasMany('App\Sub_events', 'id_events');
    }

    public function list_events() {
        return $this->hasMany('App\List_events', 'id_events');
    }
}
