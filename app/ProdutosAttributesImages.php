<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutosAttributesImages extends Model
{
    protected $table = 'ps_product_attribute_image';
}
