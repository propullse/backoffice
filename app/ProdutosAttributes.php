<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutosAttributes extends Model {
	protected $table = 'ps_product_attribute';
	protected $with = ['image'];

	public function image() {
		return $this->hasOne(ProdutosAttributesImages::class, 'id_product_attribute', 'id_product_attribute'); 	
	}

}
