<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_events extends Model
{
	protected $fillable = ['id_events', 'name'];
	protected $with = ['list_events'];

    public function list_events() {
        return $this->hasMany('App\List_events', 'id_sub_events');
    }
}
