<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('category_id');
            $table->text('title');
            $table->text('highlight_title');
            $table->text('highlight');
            $table->longText('description');

            $table->string('bgColor')->nullable();

            $table->unsignedBigInteger('section_id')->nullable();
            $table->unsignedBigInteger('section_position')->nullable();

            $table->string('header_type', 191);

            $table->string('img_alt_text')->nullable();
            $table->string('img_subtitle')->nullable();
            $table->string('img_header')->nullable();

            $table->string('gallery_alt_text')->nullable();
            $table->string('gallery_subtitle')->nullable();
            $table->longText('gallery_images')->nullable();

            $table->boolean('external_video', false)->nullable();
            $table->string('url_video')->nullable();
            $table->longText('file_video')->nullable();            
            $table->longText('image_video')->nullable();   

            $table->boolean('is_slide', false)->nullable();
            $table->boolean('published', false)->nullable();
            $table->text('tags')->nullable();
            $table->text('tags_show')->nullable();

            $table->string('slug', 191)->unique();
            $table->string('seo_title', 191);
            $table->longText('seo_description', 191);
            $table->string('seo_keywords', 191);
            $table->string('seo_img', 191);

            $table->longText('relateds')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
