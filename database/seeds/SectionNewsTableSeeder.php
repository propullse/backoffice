<?php

use App\SectionNews;
use Illuminate\Database\Seeder;

class SectionNewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SectionNews::create([
            'title' => 'areas_de_atuacao'
        ]);
        SectionNews::create([
            'title' => 'eventos_e_divulgacao'
        ]);
        SectionNews::create([
            'title' => 'inovacao_e_tecnologia'
        ]);
        SectionNews::create([
            'title' => 'news_01'
        ]);
        SectionNews::create([
            'title' => 'news_02'
        ]);
        SectionNews::create([
            'title' => 'news_03'
        ]);
    }
}
