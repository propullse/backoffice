<?php

use App\News;
use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        News::create([
            'category_id' => '1',
            'title' => '{"pt":"Transportes 4.0","en":"-"}',
            'highlight_title' => '{"pt":"Transportes 4.0","en":"-"}',
            'highlight' => '{"pt":"A maioria dos carros j\u00e1 permite que clientes e OEM\u2019s monitorizem e, at\u00e9 certo ponto, interajam com os seus ve\u00edculos.","en":"-"}',
            'description'  => '{"pt":"<div>A maioria dos carros j\u00e1 permite que clientes e OEM\u2019s monitorizem e, at\u00e9 certo ponto, interajam com os seus ve\u00edculos.&nbsp;<\/div><div>\u00c0 medida que avan\u00e7amos em dire\u00e7\u00e3o a um futuro cada vez mais aut\u00f3nomo, muitos casos de uso contam com essa conectividade&nbsp;<span style=\"color: inherit; font-family: inherit;\">e, assim, aumentam a necessidade de capacidade e confiabilidade sem fio, para que os carros se comuniquem entre si e&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">com a infraestrutura circundante, Por exemplo, j\u00e1 vemos alguns ve\u00edculos comerciais capazes de percorrer longas dist\u00e2ncias&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">sem motorista - viagens que incluem opera\u00e7\u00f5es remotas direcionando a \u00faltima milha dos seus destinos.<\/span><\/div><div><span style=\"color: inherit; font-family: inherit;\"><br><\/span><\/div><div><div style=\"\">No entanto, de uma forma geral, a infraestrutura de comunica\u00e7\u00e3o atual ainda n\u00e3o \u00e9 fi\u00e1vel o suficiente para aplica\u00e7\u00f5es&nbsp;<span style=\"color: inherit; font-family: inherit;\">de miss\u00e3o cr\u00edtica, com as redes e sistemas de comunica\u00e7\u00e3o a carecer da capacidade de gerir o tr\u00e1fego de dados de frotas&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">de carros aut\u00f3nomos que se comunicam em tempo real em ambientes urbanos densos ou ao longo de rodovias lotadas. Assim,&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">a comunica\u00e7\u00e3o direta usa a banda de espectro de sistemas de transporte inteligentes (ITS) de 5,9 GigaHertz dedicada para&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">comunica\u00e7\u00f5es de curto alcance, e n\u00e3o depende significativamente da rede celular, permitindo que os ve\u00edculos se comuniquem&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">diretamente com os arredores. Esse tipo de comunica\u00e7\u00e3o inclui a comunica\u00e7\u00e3o ve\u00edculo a ve\u00edculo (V2V), na qual os ve\u00edculos&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">comunicam para emitir avisos, evitar colis\u00f5es ou compartilhar condi\u00e7\u00f5es imediatas de estrada e tr\u00e2nsito. Tamb\u00e9m inclui a&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">comunica\u00e7\u00e3o ve\u00edculo-para-infraestrutura (V2I), onde os ve\u00edculos se comunicam com a infraestrutura, como sem\u00e1foros, sinais&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">de tr\u00e2nsito e outras infraestruturas de transporte, para fortalecer ainda mais as medidas de seguran\u00e7a. Por exemplo, um&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">sem\u00e1foro pode alertar o ve\u00edculo que ir\u00e1 ficar vermelho para que este ajuste a sua velocidade. A \u00faltima forma de comunica\u00e7\u00e3o&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">direta inclui a comunica\u00e7\u00e3o ve\u00edculo-pedestre (V2P), onde o ve\u00edculo se comunica com dispositivos transportados por pedestres&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">para garantir sua seguran\u00e7a. Por exemplo, esses dispositivos podem avisar o ve\u00edculo de que um pedestre est\u00e1 na passadeira&nbsp;<\/span><span style=\"color: inherit; font-family: inherit;\">em frente.&nbsp;<\/span><\/div><\/div>","en":"-"}',
            'bgColor' => '',
            'section_id' => '',
            'section_position' => '',
            'header_type' => 'imagem',
            'img_alt_text' => 'Stream imagem estrada',
            'img_subtitle' => 'Stream imagem estrada',
            'img_header' => 'OeErIyVhYxfEc3J54V6VOiT98XUMpTC1Z0dNKdFnL5vusO1oFXsU.jpg',
            'gallery_alt_text' => '',
            'gallery_subtitle' => '',
            'gallery_images' => '',
            'external_video' => '',
            'url_video' => '',
            'file_video' => '',
            'image_video' => '',
            'highlighted' => '0',
            'is_slide' => '0',
            'published' => '1',
            'tags' => '',
            'tags_show' => '',
            'slug' => 'transportes-4.0',
            'seo_title' => 'Transportes 4.0',
            'seo_description' => 'A maioria dos carros já permite que clientes e OEM’s monitorizem e, até certo ponto, interajam com os seus veículos.',
            'seo_keywords' => 'Stream consulting',
            'seo_img' => 'LkJZBZeIS6RnlWkPnhCkdd0sACNSrcmljzgumN9V8XXQ5h2FzvyB.jpg',
            'relateds' => '',
            'created_at' => '2020-10-26 14:20:01'
        ]);

        

        News::create([
            'category_id' => '1',
            'title' => '{"pt":"LEAN Transporte A\u00e9reo","en":"-"}',
            'highlight_title' => '{"pt":"LEAN Transporte A\u00e9reo","en":"-"}',
            'highlight' => '{"pt":"Atualmente, o transporte a\u00e9reo assume uma relev\u00e2ncia fulcral para o desenvolvimento socioecon\u00f3mico, tendo...","en":"-"}',
            'description'  => '{"pt":"<p style=\"box-sizing: border-box; color: rgb(32, 58, 113); font-size: medium; line-height: 1.6; margin-bottom: 2rem; font-family: GalanoGrotesque, sans-serif;\">Atualmente, o transporte a\u00e9reo assume uma relev\u00e2ncia fulcral para o desenvolvimento socioecon\u00f3mico, tendo tido um crescimento progressivo ao longo dos anos que se deve \u00e0 reciprocidade do desenvolvimento de atividades socioecon\u00f3micas a n\u00edvel mundial e ao grande volume de movimenta\u00e7\u00f5es de pessoas e carga a\u00e9rea. Paralelamente \u00e0s companhias a\u00e9reas, os handlers de pessoas e de carga a\u00e9rea t\u00eam tamb\u00e9m tido um grande crescimento ao longo dos \u00faltimos anos, mas para que esta ind\u00fastria possa continuar a crescer, \u00e9 de enorme relev\u00e2ncia que estas empresas procurem uma melhoria dos seus processos para prestar um servi\u00e7o com cada vez mais qualidade ao cliente final, reduzindo ao m\u00e1ximo os seus custos e desperd\u00edcios.<\/p><p style=\"box-sizing: border-box; color: rgb(32, 58, 113); font-size: medium; line-height: 1.6; margin-bottom: 2rem; font-family: GalanoGrotesque, sans-serif;\">A Groundforce Portugal \u00e9 atualmente a empresa l\u00edder do mercado de assist\u00eancia em escala ao transporte a\u00e9reo (ou seja, os handlers de pessoas e carga a\u00e9rea). Foi na \u00e1rea da carga - Groundforce Cargo - que se realizou um projeto, tendo como principal objetivo a melhoria dos processos de apoio \u00e0 exporta\u00e7\u00e3o e importa\u00e7\u00e3o de carga. Tendo como base ferramentas Lean, como o Mapeamento de Fluxo de Valor, a Gest\u00e3o Visual e o Diagrama de Esparguete, e baseando todo o racional de apresenta\u00e7\u00e3o das propostas no pensamento Lean, foram apresentadas as propostas de melhoria de forma a identificar os desperd\u00edcios e a aumentar a efici\u00eancia dos processos. Destaca-se a proposta, que visava a elimina\u00e7\u00e3o das dificuldades na procura de cargas no armaz\u00e9m de exporta\u00e7\u00e3o, ter sido implementada com resultados bastante positivos. A proposta envolveu a cria\u00e7\u00e3o de subdivis\u00f5es no ch\u00e3o, de modo a diminuir as dificuldades na procura de cargas no armaz\u00e9m, tendo tido como resultados a poupan\u00e7a de 12,11 minutos na procura de uma carga e em cerca de 214 minutos na prepara\u00e7\u00e3o de um voo.<\/p>","en":"-"}',
            'bgColor' => '',
            'section_id' => '',
            'section_position' => '',
            'header_type' => 'imagem',
            'img_alt_text' => 'Stream LEAN Transporte Aéreo',
            'img_subtitle' => 'Stream LEAN Transporte Aéreo',
            'img_header' => 'tERSE00va9gGPq3V1fNPwT8ttzdHQEKUTY0FilPbwicZuAasSE06.jpg',
            'gallery_alt_text' => '',
            'gallery_subtitle' => '',
            'gallery_images' => '',
            'external_video' => '',
            'url_video' => '',
            'file_video' => '',
            'image_video' => '',
            'highlighted' => '1',
            'is_slide' => '0',
            'published' => '1',
            'tags' => '',
            'tags_show' => '',
            'slug' => 'lean-transporte-aereo',
            'seo_title' => 'LEAN Transporte Aéreo',
            'seo_description' => 'Atualmente, o transporte aéreo assume uma relevância fulcral para o desenvolvimento socioeconómico, tendo...',
            'seo_keywords' => 'LEAN Transporte Aéreo',
            'seo_img' => 'UOfgbOuZrZm1FjuTdyPbu3avszItMTEuHhv7X6iqQwzKoH6UOVdn.jpg',
            'relateds' => '',
            'created_at' => '2020-10-26 14:56:26',
        ]);


        News::create([
            'category_id' => '2',
            'title' => '{"pt":"European Innovation Fund","en":"-"}',
            'highlight_title' => '{"pt":"European Innovation Fund","en":"-"}',
            'highlight' => '{"pt":"O Innovation Fund \u00e9 um mecanismo da Comiss\u00e3o Europeia para potenciar projetos","en":"-"}',
            'description'  => '{"pt":"<div>O Innovation Fund \u00e9 um mecanismo da Comiss\u00e3o Europeia para potenciar projetos que demonstrem tecnologias, processos ou produtos altamente inovadores, suficientemente maduros e com potencial significativo para reduzir as emiss\u00f5es de gases de efeito de estufa, em especial nas seguintes \u00e1reas:<\/div><div><div><br><\/div><ul><li>Produ\u00e7\u00e3o e uso de energia renov\u00e1vel;<\/li><li>Captura, uso e armazenamento de carbono;<\/li><li>Armazenamento de energia (incluindo componentes);<\/li><li>Produtos substitutos para ind\u00fastrias intensivas em energia.<\/li><\/ul><div><br><\/div><span style=\"color: inherit; font-family: inherit;\">Este sistema ter\u00e1 2 avisos dispon\u00edveis, tanto para <b>projetos de larga escala<\/b> como de pequena escala (candidatura a partir de 1 dezembro 2020) a implementar nos Estados Membro da Uni\u00e3o Europeia, Noruega e Isl\u00e2ndia.<\/span><br><\/div><div><br><\/div><div>A principal novidade prende-se com a atribui\u00e7\u00e3o de incentivo com taxa m\u00e1xima de 60% sobre os custos adicionais face \u00e0 tecnologia de refer\u00eancia, incorridos por cons\u00f3rcios ou a t\u00edtulo individual. Al\u00e9m disso, financiar\u00e1 ainda os custos operacionais e a perda de rentabilidade at\u00e9 10 anos.<\/div><div><br><\/div><div>Atualmente, est\u00e1 dispon\u00edvel, at\u00e9 29 de outubro de 2020, o apoio a projetos de larga escala, ou seja, com um CAPEX superior a 7,5 M\u20ac. Este aviso tem uma dota\u00e7\u00e3o de 1000 M\u20ac (~70 projetos).<\/div><div><br><\/div>","en":"-"}',
            'bgColor' => '',
            'section_id' => '',
            'section_position' => '',
            'header_type' => 'imagem',
            'img_alt_text' => 'European Innovation Fund',
            'img_subtitle' => 'European Innovation Fund',
            'img_header' => 'wWzI0XhqbAkkVfB2J17eo9dsavQFiuCGCO7HoOBrsOhoyJSvnWmY.jpg',
            'gallery_alt_text' => '',
            'gallery_subtitle' => '',
            'gallery_images' => '',
            'external_video' => '',
            'url_video' => '',
            'file_video' => '',
            'image_video' => '',
            'highlighted' => '1',
            'is_slide' => '0',
            'published' => '1',
            'tags' => '',
            'tags_show' => '',
            'slug' => 'european-innovation-fund',
            'seo_title' => 'European Innovation Fund',
            'seo_description' => 'O Innovation Fund é um mecanismo da Comissão Europeia para potenciar projetos',
            'seo_keywords' => 'European Innovation Fund',
            'seo_img' => '5hgk7mHaDgPXQhqTaBamWpniOAD3NWJPpCArHWLCVZjQl3kVpGNZ.jpg',
            'relateds' => '',
            'created_at' => '2020-10-26 15:46:15',
        ]);


        News::create([
            'category_id' => '7',
            'title' => '{"pt":"Newsletter n\u00ba 10 | Outubro 2020","en":"-"}',
            'highlight_title' => '{"pt":"Newsletter n\u00ba 10 | Outubro 2020","en":"-"}',
            'highlight' => '{"pt":"Consulte a nossa newsletter e veja mais detalhes","en":"-"}',
            'description'  => '{"pt":"<span style=\"color: rgb(32, 58, 113); font-family: GalanoGrotesque, sans-serif;\">Consulte a nossa newsletter e veja mais detalhes.<\/span><div><span style=\"color: rgb(32, 58, 113); font-family: GalanoGrotesque, sans-serif;\"><br><\/span><\/div><div><a href=\"http:\/\/streamconsulting.pt\/newsletters\/2020\/newsletter10pt.html\">http:\/\/streamconsulting.pt\/newsletters\/2020\/newsletter10pt.html<\/a><\/div>","en":"<span style=\"color: rgb(32, 58, 113); font-family: GalanoGrotesque, sans-serif;\">Consulte a nossa newsletter e veja mais detalhes.<\/span><div><span style=\"color: rgb(32, 58, 113); font-family: GalanoGrotesque, sans-serif;\"><br><\/span><\/div><div><a href=\"http:\/\/streamconsulting.pt\/newsletters\/2020\/newsletter10pt.html\">http:\/\/streamconsulting.pt\/newsletters\/2020\/newsletter10pt.html<\/a><br><\/div>"}',
            'bgColor' => '',
            'section_id' => '',
            'section_position' => '',
            'header_type' => 'imagem',
            'img_alt_text' => 'Newsletter nº 10 | Outubro 2020',
            'img_subtitle' => 'Newsletter nº 10 | Outubro 2020',
            'img_header' => 'Kajd5gVollJtGEXcOfk3VjnEeOj4wPvIXBPmDw5Im2v6DIPAb3v3.jpg',
            'gallery_alt_text' => '',
            'gallery_subtitle' => '',
            'gallery_images' => '',
            'external_video' => '',
            'url_video' => '',
            'file_video' => '',
            'image_video' => '',
            'highlighted' => '1',
            'is_slide' => '0',
            'published' => '1',
            'tags' => '',
            'tags_show' => '',
            'slug' => 'newsletter-nº-10-|-outubro-2020',
            'seo_title' => 'Newsletter nº 10 | Outubro 2020',
            'seo_description' => 'Newsletter nº 10 | Outubro 2020',
            'seo_keywords' => 'newsletter stream consulting',
            'seo_img' => '1hI8LCfT2EOvYGmdEHNzUYPaDIjCohfdzYoMHEDECsLDfv1183sg.jpg',
            'relateds' => '',
            'created_at' => '2020-10-26 15:53:54',
        ]);

    }
}
