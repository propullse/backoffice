<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'id' => '1',
            'title' => 'Insight',
            'slug' => 'insight',
            'bgColor' => 0,
        ]);
        Category::create([
            'id' => '2',
            'title' => 'Notícias',
            'slug' => 'noticias',
            'bgColor' => 0,
        ]);
        Category::create([
            'id' => '3',
            'title' => 'Eventos',
            'slug' => 'eventos',
            'bgColor' => 0,
        ]);
        Category::create([
            'id' => '4',
            'title' => 'Candidaturas abertas',
            'slug' => 'candidaturas-abertas',
            'bgColor' => 1,
        ]);
        Category::create([
            'id' => '5',
            'title' => 'Próximos eventos',
            'slug' => 'proximos-eventos',
            'bgColor' => 1,
        ]);
        Category::create([
            'id' => '6',
            'title' => 'Próximas formações',
            'slug' => 'proximas-formacoes',
            'bgColor' => 1,
        ]);
        Category::create([
            'id' => '7',
            'title' => 'Newsletter',
            'slug' => 'newsletter',
            'bgColor' => 0,
        ]);
    }
}
